import Base from './base';


class Recibo extends Base {

	All = (token, page, search, value) => {
		return this.get('recibo',{token, page, search, value});
	}

	Create = (token , data) => {
		return this.post('recibo',{token, ...data});
	}

	Get = ( token, id ) => {
		return this.get('recibo/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('recibo', {token, id});
	}

	Update = (token, data) => {
		return this.put('recibo', {...data, token});
	}

	ClientReceipts = (dni) => {

		return this.get('client-receipts/'+ dni, {});
	}
}

export default new Recibo;
