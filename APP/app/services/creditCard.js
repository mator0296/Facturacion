import Base from './base';


class CreditCard extends Base {

	All = (token,page, search, value) => {
		return this.get('creditcard',{token, page, search, value});
	}

	Create = (data) => {
		return this.post('creditcard',{...data});
	}
	Get = ( token, id ) => {
		return this.get('creditcard/' + id, {token});
	}

	Delete = (id) => {
		return this.delete('creditcard', {id});
	}

	Update = (data) => {
		return this.put('creditcard', {...data});
	}

	GetClientCards = ( dni ) => {
		return this.get('client-creditcards/', {dni});
	}
}

export default new CreditCard();
