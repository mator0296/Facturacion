import Base from './base';


class Client extends Base {

	All = (token,page) => {
		return this.get('client',{token,page});
	}

	Create = (token , data) => {
		return this.post('client',{token, ...data});
	}
	Get = ( token, id ) => {
		return this.get('client/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('client', {token, id});
	}

	Update = (token, data) => {
		return this.put('client', {...data, token});
	}

	login = (email, password)=>{
		return this.post('client-login', { email, password });
	}
}

export default new Client();
