import React from 'react';
import { Container, Text, List, ListItem } from 'native-base';
import { TouchableOpacity, View } from 'react-native';
import Firebase from '../../config/firebase';

export default class ClientsSidebar extends React.Component {

	render() {
    return (
			<Container>
			  <View style={{backgroundColor:"#2CA8FF", flex:0.25}}>
			    <Text style={{fontWeight:'bold', fontSize:25, position:'absolute', bottom:5}} > Facturacion </Text>
        </View>
        <View style={{backgroundColor:"white", flex:0.9}}>
          <List>
            <ListItem>
              <TouchableOpacity style={{width:'100%', height:'100%'}} onPress={()=>this.props.navigation.navigate("Facturas")}>
                <Text style={{textAlign:'left', width:'100%'}} >Facturas</Text>
              </TouchableOpacity>
            </ListItem>
            <ListItem>
              <TouchableOpacity style={{width:'100%', height:'100%'}} onPress={()=>this.props.navigation.navigate("Reclamos")}>
                <Text style={{textAlign:'left', width:'100%'}} >Reclamos</Text>
              </TouchableOpacity>
            </ListItem>
            <ListItem>
              <TouchableOpacity style={{width:'100%', height:'100%'}} onPress={()=>Firebase.database().ref(`closeSession`).set({value:true})}>
                <Text style={{textAlign:'left', width:'100%'}} >Cerrar sesion</Text>
              </TouchableOpacity>
            </ListItem>

          </List>
        </View>
  		</Container>
		);
	}
}
