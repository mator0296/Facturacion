import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../../config/Error';
import Firebase from '../../../config/firebase';
import CreditCard from '../../../services/creditCard';
import Select from 'react-native-picker-select';

export default class AddCreditCard extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      personales_dni:this.props.screenProps.user.user.dni,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1,
      nombretarjeta:"",
      numerotarjeta:"",
      banco:"",
      tarjeta:"",
      vencimientotarjeta:"",
      codigotarjeta:"",

    };
    if(this.props.navigation.state.params) {
      this.state = {...this.state, ...this.props.navigation.state.params.product}
    }

    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){

    if(!this.state.nombretarjeta){
      ERROR("Debe introducir un nombre","Error");
      return;
    }
    if(!this.state.numerotarjeta){
      ERROR("Debe introducir un numero","Error");
      return;
    }


    if(!this.state.banco){
      ERROR("Debe introducir un banco","Error");
      return;
    }

    if(!this.state.tarjeta){
      ERROR("Debe introducir un tipo","Error");
      return;
    }

    if(!this.state.vencimientotarjeta){
      ERROR("Debe introducir una fecha","Error");
      return;
    }

    if(!this.state.codigotarjeta){
      ERROR("Debe introducir un codigo","Error");
      return;
    }


    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      CreditCard.Update(this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateProducts`).set({value:true});
          ERROR("Tarjeta actualizada con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      CreditCard.Create(this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateProducts`).set({value:true});
          ERROR("Tarjeta creada con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }

  }

  render() {

    const data = ["Visa", "Master Card", "American Express"];

    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando Tarjeta ${this.props.navigation.state.params.product.codigo_art}` : 'Agregando Tarjeta'}</Title>
          </Body>
        </Header>
        <Content>
          <ScrollView style={styles.container}>

            <Text style={styles.label}>Tipo de tarjeta</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
      				<Select
      	        placeholder={{
      	          label: 'Seleccione',
    		          value: null,
    		        }}
    		        items={data.map(data=>({label:data, value:data, color:'black'}))}
    		        onValueChange={(value) => this.setState({tarjeta:value})}
    		        value={this.state.tarjeta}
    		      />
            </View>

            <View>
              <Text style={styles.label}>Nombre de tarjeta</Text>
              <TextInput
                style={styles.input}
                placeholder=""
                value={this.state.nombretarjeta}
                placeholderTextColor= '#ccc'
                onChangeText={(text)=>this.onChangeValue('nombretarjeta', text)}
                underlineColorAndroid={'#2CA8FF'}>

              </TextInput>

              <Text style={styles.label}>Número de tarjeta</Text>
              <TextInput
                style={styles.input}
                placeholder=""
                value={this.state.numerotarjeta}
                keyboardType='numeric'
                placeholderTextColor= '#ccc'
                onChangeText={(text)=>this.onChangeValue('numerotarjeta', text)}
                underlineColorAndroid={'#2CA8FF'}>

              </TextInput>

              <Text style={styles.label}>Expiración MM/AA</Text>
              <TextInput
                style={styles.input}
                placeholder=""
                value={this.state.vencimientotarjeta}
                placeholderTextColor= '#ccc'
                onChangeText={(text)=>this.onChangeValue('vencimientotarjeta', text)}
                underlineColorAndroid={'#2CA8FF'}>

              </TextInput>
            </View>

            <Text style={styles.label}>Código de seguridad</Text>
            <TextInput
              style={styles.input}
              placeholder=""
              keyboardType='numeric'
              placeholderTextColor='#ccc'
              value={this.state.codigotarjeta}
              onChangeText={(text)=>this.onChangeValue('codigotarjeta', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            <Text style={styles.label}>Banco</Text>
            <TextInput
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              value={this.state.banco}
              onChangeText={(text)=>this.onChangeValue('banco', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>



              <TouchableOpacity
                style={styles.button}
                onPress={this.submitValue}
                >
                {
                this.state.loading ?
                <ActivityIndicator color="#fff"/>
                :
                <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar tarjeta" : "Registrar tarjeta"}</Text>
              }
              </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />


          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />

 */
