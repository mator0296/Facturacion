import React from 'react';
import { Text, View, StyleSheet, ScrollView, TouchableHighlight, TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import Factura from '../../../services/factura';
import Firebase from '../../../config/firebase';
import factura from '../../../services/factura';
import ERROR from '../../../config/Error';

export default class ClientInvoices extends React.Component {


  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      basic: true,
      facturas:[],
      invoices:[],
      selected:[],
      style:{},
      flag:false
    };
    this.getData = this.getData.bind(this);

    this.goToFacturaDetail = this.goToFacturaDetail.bind(this);
  }
  componentDidMount() {
      this.ref = Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}`)
        .on('value', (dataSnapshot) => {
            const data = dataSnapshot.val();
            if(data){
              if(data.updateFacturas){
                this.getData();
                Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}/updateFacturas`).set(null);
              }
            }

        });
      this.getData();
  }

  getData(){
    this.setState({refreshing:true})
    factura.ClientInvoices(this.props.screenProps.user.user.dni)
      .then(data=>{
        this.setState({refreshing:false ,facturas: data ? data : [],invoices: data ? data.reverse() : []})
      });


  }

  goToFacturaDetail(factura) {
    const invoices = this.state.invoices.filter(data=>((data.id !== factura.id )&& (data.estado === '0')));
    
    this.props.navigation.navigate('InvoiceDetails', {factura, invoices})
  }

  payInvoices() {
    let selected = this.state.selected;
    let invoices = this.state.invoices.filter(data=>((data.id !== selected.id )&& (data.estado === '0')));
    
    if (!invoices.length) {
      ERROR("No hay facturas por pagar!!", "Error");
      return;
    }
    this.props.navigation.navigate('PayInvoices', {selected, invoices})
  }

  selectInvoice(invoice, j){

    if (this.state.style[j]) {
      this.state.selected = this.state.selected.filter((data,i)=>(data['id']!==j));
      this.state.invoices.push(invoice);
    } else {
      this.state.selected.push(invoice);
      this.state.invoices = this.state.invoices.filter((data,i)=>(data['id']!==j));
    }
    this.state.style[j] = !this.state.style[j];

    this.setState({flag:Boolean(this.state.selected.length)})

  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.facturas];
    const dataDelete = newData.splice(rowId, 1)[0]
    factura.Delete(this.props.screenProps.user.token,dataDelete.id)
      .then(data=>console.log(data))
    this.setState({ facturas: newData });
  }

  Estado(factura) {
    console.log(factura.estado, factura.pagado)
    if (factura.estado === '0' && factura.pagado === '0') 
      return 'No pagado';
    if (factura.pagado === '0') 
      return 'Pago en proceso';
    return 'Pagado' 
  }

  ColorEstado(factura) {
     if (factura.estado === '0' && factura.pagado === '0') 
      return 'red';
    if (factura.pagado === '0') 
      return 'yellow';
    return 'green';
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Facturas</Title>
          </Body>
          <Right>
            <Button onPress={this.payInvoices.bind(this)} transparent>
              <Text style={{fontSize:16, color:'#FFF', paddingBottom:1}}>PAGAR &nbsp;</Text>
              <Icon type="Octicons" name="chevron-right" />
            </Button>
          </Right>
        </Header>
        <Content>
          <List
            dataSource={this.ds.cloneWithRows(this.state.facturas)}
            renderRow={(data,i) =>
              <TouchableHighlight key={i} style={[styles.evento, this.state.style[data['id']] ? styles.selected : {}, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onLongPress={() =>{if(data.estado === '1') return; this.selectInvoice(data, data['id'])}} underlayColor="white"
                onPress={()=>{ !this.state.flag?this.goToFacturaDetail(data):this.selectInvoice(data, data['id'])}}>
                <View style={{paddingLeft:10}}>
                  { data.num_factura ?
                    <Text style={styles.eventoTitulo}>{`Numero: ${data.num_factura}`}</Text>
                  : null }
                    <Text style={styles.eventoDescripcion}>{`Descripcion: ${data.descripcion}`}</Text>
                    <Text style={styles.eventoFecha}>
                      {`Total: ${data.debe}`} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {`Cantidad: 1`} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <Text style={{color:this.ColorEstado(data)}}>{this.Estado(data)}</Text>
                    </Text>
                </View>
              </TouchableHighlight>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('FacturaCreate', {factura:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToFacturaDetail(data)}>
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },
  selected:{
    backgroundColor:'#DDDDDD',
  },
  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});
