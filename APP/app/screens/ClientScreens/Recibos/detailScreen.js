import React from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Text, Linking } from 'react-native';
import { Container, Header, Right, Left, Title, Body, Content, Button, Icon, Thumbnail } from 'native-base';
import Firebase from '../../../config/firebase';
import ERROR from '../../../config/Error';

export default class ReceiptDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          starCount: 0,
          proposal:[],
          isAcepted:false,
          proposalAcepted:{},
          isModal:false,
          isModalPay:false
        };

        this.acceptProposal = this.acceptProposal.bind(this);
    }

    onStarRatingPress(rating) {
        this.setState({
          rating
        });
    }

    acceptProposal(proposal) {
        Proposal.updateStatus(1, proposal.users_id, this.props.navigation.state.params.event.id);
        proposal.status = 1;
        this.setState({proposalAcepted:proposal, isAcepted:true});
        Firebase.database().ref(`chat/${this.props.screenProps.user.user['\ufeffid']}/${this.props.navigation.state.params.event.id}`).set({proposal, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user['\ufeffid'], proposalOnwerId: proposal.users_id, message:[]});
        Firebase.database().ref(`chat/${proposal.users_id}/${this.props.navigation.state.params.event.id}`).set({proposal, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user['\ufeffid'], proposalOnwerId: proposal.users_id, message:[]});
    }



    makePayment(){
        Proposal.updateStatus(3, this.state.proposalAcepted.users_id, this.props.navigation.state.params.event.id);
        this.setState({proposalAcepted:{...this.state.proposalAcepted, status:3}, isModalPay:false});
        Firebase.database().ref(`chat/${this.props.screenProps.user.user['\ufeffid']}/${this.props.navigation.state.params.event.id}/proposal`).set(this.state.proposalAcepted);
        Firebase.database().ref(`chat/${this.state.proposalAcepted.users_id}/${this.props.navigation.state.params.event.id}/proposal`).set(this.state.proposalAcepted);
        Firebase.database().ref(`update/${this.props.screenProps.user.user['\ufeffid']}/updateEvents`).set({value:true});
        ERROR("Evento finalizado con éxito","exito");
        this.props.navigation.goBack();
    }

    finishWork(){
        Proposal.updateStatus(2, this.state.proposalAcepted.users_id, this.props.navigation.state.params.event.id, this.state.rating);
        this.setState({proposalAcepted:{...this.state.proposalAcepted, status:2}, isModal:false});
        Firebase.database().ref(`chat/${this.props.screenProps.user.user['\ufeffid']}/${this.props.navigation.state.params.event.id}/proposal`).set(this.state.proposalAcepted);
        Firebase.database().ref(`chat/${this.state.proposalAcepted.users_id}/${this.props.navigation.state.params.event.id}/proposalweew`).set(this.state.proposalAcepted);
        if(!this.state.isModalPay){
            Firebase.database().ref(`update/${this.props.screenProps.user.user['\ufeffid']}/updateEvents`).set({value:true});
            ERROR("Evento finalizado con éxito","exito");
            this.props.navigation.goBack();
        }

    }
    render() {

        return (
            <Container>
                <Header style={{backgroundColor:'#2CA8FF'}}>
                <Left>
                    <Button onPress={()=>this.props.navigation.goBack()} transparent>
                    <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title>{`Recibo ${this.props.navigation.state.params.recibo.id}`}</Title>
                </Body>
                <Right>

                </Right>
                </Header>
                <Content>
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{alignItems:'center', marginTop:'5%'}}>
                                <Thumbnail style={{width:100, height:100, borderColor:'#888', borderWidth:1, padding:5}} source={require('../../../img/product.png')}/>
                            </View>
                            <View style={styles.evento}>
                                <Text style={styles.eventoTitulo}>{`Numero: ${this.props.navigation.state.params.recibo.id}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Detalles: ${this.props.navigation.state.params.recibo.descripcion}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Fecha: ${this.props.navigation.state.params.recibo.fecha.split(' ')[0]}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`importe: ${this.props.navigation.state.params.recibo.importe}`}</Text>
                            </View>
                            <View style={{flexDirection:'row',justifyContent:'center'}}>
                                <TouchableOpacity
                                  style={styles.buttonNext}
                                  onPress={()=>{ERROR("En Construcción","Error")}}
                                >
                                    <Text style={styles.buttonTextModal}>{"Ver recibo"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
      }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,

    },

    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        marginBottom:20,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    proveedor : {
        padding: 20,
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 1,
        backgroundColor: "#fff",
    },

    proveedorNombre : {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: 'normal',
        color: "#DEB749"
    },

    proveedorDescripcion : {

    },

    proveedorNombrePrecio: {
        flexDirection: 'row',
        justifyContent:'space-between',

    },

    proveedorPrecio : {
        fontSize: 18,
        color: "#ea1d75",
        fontWeight: 'bold'
    },

    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    buttonNext : {
        height: 40,
        backgroundColor:"#888",
        alignSelf: 'stretch',
        width:'35%',
        marginLeft:'10%',
        marginBottom:'5%'
    },

    buttonText : {
        paddingTop: 5,
        color: "#ea1d75",
        textAlign:'center',
        fontSize: 17,
    },

    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
        paddingVertical:10
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'80%',
        alignSelf: 'stretch',

        marginLeft:'10%',
        borderRadius: 3,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:250,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:30,
        height:30
    }

});
