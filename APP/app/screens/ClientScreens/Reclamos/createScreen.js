import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../../config/Error';
import Firebase from '../../../config/firebase';
import Reclamo from '../../../services/reclamos';
import Client from '../../../services/client';
import Facturas from '../../../services/factura';
import Select from 'react-native-picker-select';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';

export default class CreateTicket extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      token:this.props.screenProps.user.token,
      personales_dni:this.props.screenProps.user.user.dni,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1,
      clients:[],
      facturas:[],
      datachargue:true,
    };
    if(this.props.navigation.state.params) {
      this.state.datachargue = false;
      this.state = {...this.state, ...this.props.navigation.state.params.reclamo}
      Facturas.ClientInvoices(this.state.personales_dni)
        .then(data=>{
          this.setState({refreshing:false ,facturas:data?data:[]})
          console.log(this.state.facturas)
        });
    }

    Facturas.ClientInvoices(this.state.personales_dni)
      .then(data=>{
        this.setState({refreshing:false ,facturas:data?data:[]})
        console.log(this.state.facturas[0].id)
      });

    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  componentWillMount(){
    console.log(this.state)
  }

  submitValue(){

    if(!this.state.descripcion){
      ERROR("Debe introducir una descripcion","Error");
      return;
    }

    if(!this.state.personales_dni){
      ERROR("Debe elegir un cliente","Error");
      return;
    }

    if(!this.state.facturas_id){
      ERROR("Debe elegir una factura","Error");
      return;
    }


    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      Reclamo.UpdateClientTicket(this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}/updateReclamos`).set({value:true});
          ERROR("Reclamo actualizado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      Reclamo.CreateClientTicket(this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}/updateReclamos`).set({value:true});
          ERROR("Reclamo creado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log('error',err)
          this.setState({loading:false})
        })
    }

  }

  render() {

    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando reclamo ${this.props.navigation.state.params.reclamo.id}` : 'Creando Reclamo'}</Title>
          </Body>
        </Header>
        <Content>
          <ScrollView style={styles.container}>

            <Text style={styles.label}>Descripcion</Text>
            <TextInput
              style={styles.input}
              placeholder=""
              value={this.state.descripcion}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('descripcion', text)}
              underlineColorAndroid={'#2CA8FF'}>

            </TextInput>

            <Text style={styles.label}>Facturas</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona una factura...',
                      value: null,
                  }}
                  items={this.state.facturas.map(data=>({label:data.descripcion, value:data['id'], color:'black'}))}
                  onValueChange={(value) => this.onChangeValue('facturas_id', value)}
                  value={this.state.facturas_id}

              />
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={this.submitValue}
              >
              {
              this.state.loading ?
              <ActivityIndicator color="#fff"/>
              :
              <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar reclamo" : "Registrar reclamo"}</Text>
            }
            </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />


          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});
