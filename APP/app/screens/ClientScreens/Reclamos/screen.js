import React from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import Reclamos from '../../../services/reclamos';
import Firebase from '../../../config/firebase';

export default class Tickets extends React.Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      basic: true,
      reclamos:[]
    };
    this.getData = this.getData.bind(this);

    this.goToReclamoDetail = this.goToReclamoDetail.bind(this);
  }
  componentWillMount() {
      this.ref = Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}`)
        .on('value', (dataSnapshot) => {
            const data = dataSnapshot.val();
            if(data){
              if(data.updateReclamos){
                this.getData();
                Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}/updateReclamos`).set(null);
              }
            }

        });
      this.getData();
  }


  getData(){
    this.setState({refreshing:true})
    Reclamos.GetClientTickets(this.props.screenProps.user.user.dni)
      .then(data=>{
        this.setState({refreshing:false ,reclamos: data ? data.reverse() : []})
      });
  }

  goToReclamoDetail(reclamo) {

      this.props.navigation.navigate('TicketDetails', {reclamo})


  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.reclamos];
    const dataDelete = newData.splice(rowId, 1)[0]
    Reclamos.Delete(this.props.screenProps.user.token,dataDelete.id)
      .then(data=>console.log(data))
      .catch(error=>console.log(error))
    this.setState({ reclamos: newData });
  }

  render() {
    console.log(this.state.reclamos)
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>{"Reclamos"}</Title>
          </Body>
          <Right>

          </Right>
        </Header>
        <Content>
          <List
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(this.state.reclamos)}
            renderRow={(data,i) =>
              <TouchableOpacity key={i} style={[styles.evento, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onPress={()=>this.goToReclamoDetail(data)}>
                <Text style={styles.eventoTitulo}>{`Numero: ${data.id}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Descripcion: ${data.descripcion}`}</Text>
              </TouchableOpacity>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('CreateTicket', {reclamo:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={() => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
        <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate('CreateTicket')}>
            <Icon name="md-add" />

          </Fab>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToReclamoDetail(data)}>
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});
