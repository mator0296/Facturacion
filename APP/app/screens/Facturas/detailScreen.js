import React from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Text } from 'react-native';
import { Container, Header, Right, Left, Title, Body, Content, Button, Icon, Thumbnail } from 'native-base';
import Firebase from '../../config/firebase';


export default class EventoDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          starCount: 0,
          proposal:[],
          isAcepted:false,
          proposalAcepted:{},
          isModal:false,
          isModalPay:false
        };
         
        
    }
    
    onStarRatingPress(rating) {
        this.setState({
          rating
        });
    }  

   
    render() {
        
        return ( 
            <Container>
                <Header style={{backgroundColor:'#2CA8FF'}}>
                <Left>
                    <Button onPress={()=>this.props.navigation.goBack()} transparent>
                    <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title>{`Factura ${this.props.navigation.state.params.factura.num_factura}`}</Title>
                </Body>
                <Right>
                
                </Right>
                </Header>    
                <Content>
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{alignItems:'center', marginTop:'5%'}}>
                                <Thumbnail style={{width:100, height:100, borderColor:'#888', borderWidth:1, padding:5}} source={require('../../img/client.png')}/> 
                            </View>
                            <View style={styles.evento}>
                                <Text style={styles.eventoTitulo}>{`Numero: ${this.props.navigation.state.params.factura.num_factura}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Por : ${this.props.navigation.state.params.factura.importe_pagado}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Descripcion: ${this.props.navigation.state.params.factura.descripcion}`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Estatus: ${this.props.navigation.state.params.factura.estado}`}</Text>
                                <Text style={styles.eventoFecha}>{`Creado: ${this.props.navigation.state.params.factura.created_at.split(' ')[0]}`}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity 
                                    style={styles.buttonNext}
                                    onPress={()=>this.props.navigation.navigate('FacturaCreate', {factura:this.props.navigation.state.params.factura})}
                                    >
                                    <Text style={styles.buttonTextModal}>{"Editar"}</Text>  
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={styles.buttonNext}
                                    onPress={()=>this.setState({isModal:true})}
                                    >
                                    <Text style={styles.buttonTextModal}>{"Eliminar"}</Text>  
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
      }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        
    },

    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        marginBottom:20,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    proveedor : {
        padding: 20,
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 1,
        backgroundColor: "#fff",
    },
    
    proveedorNombre : {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: 'normal',
        color: "#DEB749"
    },
    
    proveedorDescripcion : {
    
    },

    proveedorNombrePrecio: {
        flexDirection: 'row',
        justifyContent:'space-between',
        
    },

    proveedorPrecio : {
        fontSize: 18,
        color: "#ea1d75",
        fontWeight: 'bold'
    },

    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    buttonNext : {
        height: 40,
        backgroundColor:"#888",
        alignSelf: 'stretch',
        width:'35%',
        marginLeft:'10%',
        marginBottom:'5%'
    },

    buttonText : {
        paddingTop: 5,
        color: "#ea1d75",
        textAlign:'center',
        fontSize: 17,
    },

    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
        paddingVertical:10
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'80%',
        alignSelf: 'stretch',
       
        marginLeft:'10%',
        borderRadius: 3,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:250,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:30,
        height:30
    }
  
});


