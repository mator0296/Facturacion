import React from 'react';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

import Ionicons from "react-native-vector-icons/Ionicons";

import Categories from '../services/categories';

export default class ProveedorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      categories:[],
    };
    Categories.getCategories()
            .then(data=>this.setState({categories:data ? data : []}))
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.wrapper}>
        {this.state.categories.map((data, i)=>(
              <TouchableOpacity key={i} style={styles.proveedorCategoria} onPress={() => this.props.navigation.navigate('DetailProveedores', {categories_id:data.id})}>
                <Ionicons name={data.iconapp} size={50} color={"#e58e26"} />
                <Text style={styles.proveedorNombre}>{data.name}</Text>
              </TouchableOpacity>

          ))}

      </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  wrapper: {
    flex: 1, 
    flexDirection: 'row',
    padding: 10,
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },

  proveedorCategoria: {
    //width: 50,
    width: "44%",
    //height: 100, 
    //flex: 0.5,
    backgroundColor: 'powderblue',
    margin: 10,
    alignItems: 'center',

    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    shadowRadius: 10,
    backgroundColor: "#fff",
    padding: 20,
    // borderBottomColor: "#ea1d75",
    // borderBottomWidth: 3,

  },
  

  proveedorNombre : {
    fontSize: 17,
    marginTop: 5,
    fontWeight: 'normal',
    color: "#212121",
    textAlign:"center"
  },

  proveedorDescripcion : {

  }

});


