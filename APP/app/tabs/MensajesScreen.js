import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import Firebase from '../config/firebase';

export default class MensajesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      chat:[]
    };
    this.ref = Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`)
        .on('value', (dataSnapshot) => { 
           
            this.setState({chat: dataSnapshot.val() ? dataSnapshot.val() : []})
            
        });
    
    
  }
  componentWillUnmount() {
    Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`).off('value', this.ref)
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {
          Object.keys(this.state.chat).map((idEvent,i)=>(
            <TouchableOpacity style={styles.mensaje} onPress={() => this.props.navigation.navigate('DetailMensaje', {idEvent})}>
              <View style={styles.mensajeWrapAvatar}>
                <Image style={styles.mensajeAvatar} source={i%2 === 0 ? require('../img/avatar.png'):require('../img/avatar_1.jpg' )}/> 
              </View>

              <View style={styles.mensajeInfo}>
                <Text style={styles.mensajeNombre}>{this.state.chat[idEvent].proposal.users.profile.name}</Text>
                <Text style={styles.mensajeNombreProyecto}>{this.state.chat[idEvent].event.name}</Text>
              </View>

              <Ionicons name={'ios-arrow-forward'} size={25} color={"#DEB749"} />

            </TouchableOpacity>
          ))
          
        }
          
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: "#fff",
  },

  mensaje : {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
    padding: 20,
    height: 90,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
  },

  mensajeWrapAvatar : {
    borderRadius: 100,
    width: 60,
    height: 60,
    marginRight: 20,
  },
  mensajeAvatar : {
    width: 60,
    height: 60,
    marginBottom: 50,
    resizeMode: 'contain',
    borderRadius: 30,
    
  },

  mensajeInfo : {
    
    height: 60,
    width: "70%",

  },

  mensajeNombre : {
    fontSize: 17,
    marginBottom:5,
  },

  mensajeNombreProyecto : {
    color: "gray",
    fontSize: 15,
  },



});


