-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-09-2018 a las 02:44:53
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `acces_facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_det`
--

CREATE TABLE `articulo_det` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `codigo_art` varchar(45) DEFAULT NULL,
  `detalle_art` varchar(45) DEFAULT NULL,
  `unitario` varchar(45) DEFAULT NULL,
  `porcentaje` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo_det`
--

INSERT INTO `articulo_det` (`id`, `codigo_art`, `detalle_art`, `unitario`, `porcentaje`) VALUES
(1, 'aa111', 'prueba', '122', '11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credit_card`
--

CREATE TABLE `credit_card` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `debito` text,
  `banco` text,
  `tarjeta` text,
  `nombretarjeta` text,
  `numerotarjeta` text,
  `vencimientotarjeta` text,
  `codigotarjeta` text,
  `codmes` int(11) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `personales_dni` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `personales_dni` int(11) DEFAULT NULL,
  `facturas_id_factura` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas_has_articulo_det`
--

CREATE TABLE `facturas_has_articulo_det` (
  `facturas_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `facturas_personales_id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT '0',
  `articulo_det_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `﻿id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_factura` int(11) DEFAULT NULL,
  `codcliente` int(11) DEFAULT NULL,
  `codplan` int(11) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `nombre` text,
  `cod_producto` int(11) DEFAULT NULL,
  `num_factura` int(11) DEFAULT NULL,
  `descripcion` text,
  `vencimiento` text,
  `mora` int(11) DEFAULT NULL,
  `vencimiento2` int(11) DEFAULT NULL,
  `debe` text,
  `saldo` int(11) DEFAULT NULL,
  `observada` text,
  `leyendaobservada` text,
  `clase` text,
  `haber` text,
  `hora` text,
  `fecha_sistema` text,
  `cod_mes` text,
  `pagado` int(11) DEFAULT NULL,
  `periodo` text,
  `fecha_pago` text,
  `importe_pagado` text,
  `recibo` text,
  `codoperario` int(11) DEFAULT NULL,
  `forma` text,
  `estado` text,
  `entrega` text,
  `bon_intereses` text,
  `observaciones` text,
  `pagosraul` text,
  `fechapagoraul` text,
  `reciboraul` text,
  `sergiopagado` text,
  `observacionessergio` text,
  `codenviado` int(11) DEFAULT NULL,
  `ZONA` int(11) DEFAULT NULL,
  `tipo` text,
  `cae` text,
  `caevencimiento` text,
  `caeentregado` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personales`
--

CREATE TABLE `personales` (
  `﻿id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `dni` int(11) DEFAULT NULL,
  `nombre` text,
  `domicilio` text,
  `telefono` text,
  `email` text,
  `email2` text,
  `Barrio` text,
  `codigo_postal` text,
  `ciudad` text,
  `zona` int(11) DEFAULT NULL,
  `cod_barrio` int(11) DEFAULT NULL,
  `provincia` text,
  `servidor` text,
  `nodo` text,
  `ubicacion_nodo` text,
  `numero_servidor` int(11) DEFAULT NULL,
  `servidor_secundario` int(11) DEFAULT NULL,
  `tipo_equipo` text,
  `equipo` text,
  `ip` text,
  `ipequipo` text,
  `mac` text,
  `fecha_alta` text,
  `hora` text,
  `velocidad` int(11) DEFAULT NULL,
  `baja` text,
  `cuota` int(11) DEFAULT NULL,
  `bajadetalle` text,
  `baja_provisoria` text,
  `fecha_provisoria` text,
  `fecha_baja` text,
  `fecha_nac` text,
  `tolerancia` int(11) DEFAULT NULL,
  `fecha_instalacion` text,
  `chacra` text,
  `tolhuin` text,
  `instalar` text,
  `fecha_instalado` text,
  `obs_instala` text,
  `centro_ventas` int(11) DEFAULT NULL,
  `vencimiento_factura` int(11) DEFAULT NULL,
  `errorcorreo` text,
  `trabajo` text,
  `contrato` text,
  `orden` text,
  `condeuda` text,
  `baja_equipo` text,
  `ok` text,
  `barrapersonal` text,
  `adios` text,
  `comopaga` text,
  `red` text,
  `idap` text,
  `ap` text,
  `idservidor` int(11) DEFAULT NULL,
  `idantena` text,
  `tipofactura` text,
  `tipodoc` text,
  `codtipodoc` int(11) DEFAULT NULL,
  `condicioniva` text,
  `empresa` text,
  `debito` text,
  `banco` text,
  `tarjeta` text,
  `nombretarjeta` text,
  `numerotarjeta` text,
  `vencimientotarjeta` text,
  `codigotarjeta` text,
  `codmes` int(11) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibos`
--

CREATE TABLE `recibos` (
  `﻿id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `codfactura` int(11) DEFAULT NULL,
  `fecha` text,
  `recibo` text,
  `codmes` text,
  `codproducto` int(11) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `nombre` text,
  `descripcion` text,
  `importe` text,
  `importe_original` text,
  `intereses` text,
  `operador` text,
  `codmes_fecha` text,
  `zona` int(11) DEFAULT NULL,
  `codoperacion` int(11) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `forma` text,
  `comprobante_tarjeta` int(11) DEFAULT NULL,
  `TIPO` text,
  `lote` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `permiso` varchar(45) DEFAULT NULL,
  `administrador` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `nombre`, `clave`, `permiso`, `administrador`) VALUES
(1, 'admin', 'admin', '123456', '1', '1');

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
