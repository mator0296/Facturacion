import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class Product extends Base {

	All = (token, page) => {
		return this.get('product',{token, page});
	}

	Create = (token , data) => {
		return this.post('product',{token, ...data});
	}
	Get = ( token, id ) => {
		return this.get('product/' + id, {token});
	}

	Delete = (token, codigo_art) => {
		console.log(codigo_art)
		return this.delete('product', {token, codigo_art});
	}

	Update = (token, data) => {
		return this.put('product', {...data, token});
	}
}

export default new Product(server)