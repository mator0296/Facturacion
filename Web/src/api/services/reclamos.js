import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class Recibo extends Base {

	All = (token, page) => {
		return this.get('reclamos',{token, page});
	}

	Create = (token , data) => {
		return this.post('reclamos',{token, ...data});
	}
	
	Get = ( token, id ) => {
		return this.get('reclamos/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('reclamos', {token, id});
	}

	Update = (token, data) => {
		return this.put('reclamos', {...data, token});
	}
}

export default new Recibo(server)
