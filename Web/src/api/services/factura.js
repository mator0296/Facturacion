import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class Factura extends Base {

	All = (token, page, search, value) => {
		return this.get('factura',{token, page, search, value});
	}

	Create = (token , data) => {
		return this.post('factura',{token, ...data});
	}
	
	Get = ( token, id ) => {
		return this.get('factura/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('factura', {token, id});
	}

	Update = (token, data) => {
		return this.put('factura', {...data, token});
	}

	FacturaForClient = (token, id) => {
		return this.get('facturaforclient/'+ id, {token});
	}

	ProductsFactura = (token ,id) => {
		return this.get('productsfactura', {token,id});
	}

	IvaFactura = () => {
		return this.get('afip/getIvaFactura', {});
	}

	GetNum = () => {
		return this.get('afip/getNumFactura', {});
	}

	GetType = () => {
		return this.get('afip/getDocFactura', {});
	}

	GetDataNewFactura = () => {
		return this.get('afip/getDataNewFactura', {});
	}

	registerAfip = (id) => {
		return this.get('afip/'+id, {});
	}

	openMercadoPago = (id) => {
		return this.get('mercadoPago/'+id,{});
	}

	registerFacturas = (data) => {
		return this.post('registerFacturas',data);
	}

	
}

export default new Factura(server)