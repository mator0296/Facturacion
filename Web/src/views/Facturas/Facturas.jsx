import React from 'react';
import swal from 'sweetalert';
import {
    Button,
    Row,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    InputGroup,
    FormGroup,
    Table,
    Card,
    CardBody,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink
} from 'reactstrap';

import Loader from 'react-loader-spinner';
import {
    PanelHeader,
    Checkbox,
    Radio
} from '../../components';

import ModalFactura from './recibosModal';
import language from "../../api/translator/translator";
import Cookies from 'react-cookies';
import Client from '../../api/services/client';
import User from '../../api/services/user';
import Product from '../../api/services/product';
import Factura from '../../api/services/factura';
import CreditCard from '../../api/services/creditCard';

class Facturas extends React.Component{

  constructor(props) {
    super(props);
    let today = new Date();
    let dd = today.getDate() < 10 ? '0'+today.getDate() : today.getDate();
    let mm = today.getMonth()+1 < 10 ? '0'+(today.getMonth()+1) : (today.getMonth()+1);
    let yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;

    this.state = {visible: false,
                  toggle: false,
                  notifications:[],
                  clients:[],
                  products:[],
                  dni:"",
                  addProduct:false,
                  selectProduct:false,
                  User:Cookies.load('userId'),
                  users:[],
                  productsSelect:[],
                  typeFactura:[],
                  importe_pagado:0,
                  indexProduct:-1,
                  facturas:[],
                  createFacturaModal:false,
                  estado:0,
                  cantidad:0,
                  filterUser:{},
                  page:1,
                  tipo:'Efectivo',
                  total:[],
                  search:undefined,
                  value:undefined,
                  mora:0,
                  Ivas:[],
                  typesDocument:[],
                  bon_intereses:0,
                  haber:0,
                  pagado:0,
                  creditCardModal:false,
                  errClient:'',
                  creditCards:[],
                  loading:true,
                  checked:{},
                  vencimiento:today
                 };

    if (!Cookies.load('userId'))
      window.location.href="/login"
    this.toggleModal = this.toggleModal.bind(this);
    this.openCreditCardModal = this.openCreditCardModal.bind(this);
    Client.All(this.state.User.access_token)
      .then(data=>this.setState({clients: data ? data.filter((data)=>(!Number(data.baja))) : []}));
    Product.All(this.state.User.access_token)
      .then(data=>this.setState({products: data ? data : []}));
    User.getUsers(this.state.User.access_token)
      .then(data=>this.setState({users: data ? data : []}));
    this.getData(this.state.page);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    this.updateData = this.updateData.bind(this);


  }
  async getData(page, search, value){
    Factura.All(this.state.User.access_token,page, search, value)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)
          }
          console.log(data)
          this.setState({facturas: data  ? data.data : [], total})
        });


  }

  componentDidMount() {
    Factura.GetDataNewFactura()
      .then((data)=>{
        this.setState({ typesDocument:data.typeDocument, Ivas:data.iva, typeFactura:data.typeFactura, loading:false});
      });
  }


  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {

    this.setState({
      createFacturaModal: !this.state.createFacturaModal,
      updateProduct:false,
      disabled:false,
      index:null
    });
  }

  addProducts(){

    if (this.state.dni === '')return;

    console.log(Number(this.state.facturas[this.state.indexProduct].estado))
    if(Number(this.state.facturas[this.state.indexProduct].estado) > 0) {
      return ;
    }

    this.state.productsSelect.push({...this.state.facturas[this.state.indexProduct], cantidad:1})
    let importe_pagado = 0;
    for (var i = this.state.productsSelect.length - 1; i >= 0; i--) {

      importe_pagado += Number(this.state.productsSelect[i].debe.replace(',','.')) * this.state.productsSelect[i].cantidad;
    }
    let iva = this.state.Ivas[this.state.Ivas.map(data=>data.Id).indexOf(this.state.bon_intereses)].Desc;
    iva = iva.replace("%","");
    let importe_pagado_old = importe_pagado;
    importe_pagado = importe_pagado + ((importe_pagado*iva)/100);

    this.setState({selectProduct:false, importe_pagado_old, importe_pagado, indexProduct:-1, cantidad:0, checked:{...this.state.checked, [this.state.facturas[this.state.indexProduct]['\ufeffid']]:true}})



  }


  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1, this.state.search, this.state.value);
    this.setState({page:this.state.page+1});

  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1, this.state.search, this.state.value);
    this.setState({page:this.state.page-1});

  }

  goToPage(index) {
    console.log(this.state.search, this.state.value)
    this.getData(index, this.state.search, this.state.value);
    this.setState({page:index});

  }


  createFactura(){
    console.log(this.state)
    if(!this.state.dni  || this.state.tipo === '0'  || this.state.productsSelect.length === 0 ) {
      swal(language("ErrorCampo"), {icon:"warning", button:false, timer:1500})
      return;
    }

    if(this.state.pagado > this.state.importe_pagado){
      swal(language("MontoSuperaDeuda"), {icon:"warning", button:false, timer:1500})
      return;
    }

    let haber = this.state.importe_pagado - this.state.pagado;

    this.state.haber = haber.toFixed(2);
    this.state.saldo = this.state.importe_pagado - this.state.haber;
    this.state.mora = this.state.haber;

    this.setState({loading:true, disabled:true})

    Factura.registerFacturas(this.state)
      .then((data)=>{
        this.setState({loading:false, disabled:false})
        var a = document.createElement("a");
        a.href = data.url;
        a.target = '_black'
        a.click();

        swal(language("Factura") + " " + language("MsjCreadoExito"),{icon:"success", button:false, timer:2000})
        this.setState({importe_pagado:0, disabled:false,checked:{}, loading:false,pagado:0, mora:0, haber:0, createFacturaModal:false, productsSelect:[], cantidad:0, descripcion:null, num_factura:null})
        this.goToPage(1);
      })
  }

  encontruccion(){
     swal(language("EnConstruccion"),{icon:"warning", button:false, timer:1500})
  }

  deleteFactura(index) {
    swal({
      title: language("MsjEliminarProducto") + ` ${this.state.Products[index].codigo_art} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      showCancelButton:true,
      button: {
        text:language("BotonEliminar"),
        className:'btn-danger',
        onClick:(()=>{console.log("click")})
      },
      closeOnConfirm:false
    }).then(willDelete=>{
      if(!willDelete) return;
      Factura.Delete(this.state.User.access_token, this.state.users[index].id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjUsuarioEliminado") +` ${this.state.users[index].nombre}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({users:this.state.users.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.users[index].nombre}`, 'warning')
        })

    })


  }


  openUpdate(index){
    Factura.ProductsFactura(this.state.User.access_token,this.state.facturas[index].id)
      .then(data=>this.setState({productsSelect:data}))
    this.setState({...this.state.facturas[index], createFacturaModal:true, updateFactura:true, indexToEdit:index})
  }


  updateFactura(){
    if(!this.state.dni || !this.state.vencimiento || !this.state.descripcion  || this.state.productsSelect.length === 0 ) {
      swal(language("ErrorCampo"), {icon:"warning", button:false, timer:1500})
      return;
    }

    if(this.state.pagado > this.state.importe_pagado){
      swal(language("MontoSuperaDeuda"), {icon:"warning", button:false, timer:1500})
      return;
    }

    let haber = this.state.importe_pagado - this.state.pagado;

    this.state.haber = haber.toFixed(2);
    this.state.saldo = this.state.importe_pagado - this.state.haber;
    this.state.mora = this.state.haber;
    this.setState({loading:true,disabled:true});
    Factura.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        this.state.facturas[this.state.indexToEdit] = data;
        this.toggleModal();
        swal(language("Factura") + `${this.state.num_factura} actualizado con exito`,{icon:"success", button:false, timer:2000})
        this.setState({loading:false, disabled:false})
      })

  }

  goToRecibos(){
    window.location.href='/recibos';
  }

  toggleRecibos(index) {
    this.setState({openReciboModal:!this.state.openReciboModal, indexReciboModal:index})
  }

  deleteProduct(index) {
    this.state.productsSelect = this.state.productsSelect.filter((data,i)=>i !== index);
    let importe_pagado = 0;
    for (var i = this.state.productsSelect.length - 1; i >= 0; i--) {

      importe_pagado += Number(this.state.productsSelect[i].debe.replace(',','.')) * this.state.productsSelect[i].cantidad;
    }
    let iva = this.state.Ivas[this.state.Ivas.map(data=>data.Id).indexOf(this.state.bon_intereses)].Desc;
    iva = iva.replace("%","");
    importe_pagado = importe_pagado + ((importe_pagado*iva)/100);
    this.setState({importe_pagado,checked:{...this.state.checked, [this.state.facturas[index]['\ufeffid']]:false}});
  }

  editProduct(index) {
    const deleteProduct = this.state.productsSelect[index];
    this.state.productsSelect = this.state.productsSelect.filter((data,i)=>i !== index);
    let importe_pagado = 0;
    for (var i = this.state.productsSelect.length - 1; i >= 0; i--) {
      importe_pagado += this.state.productsSelect[i].unitario * this.state.productsSelect[i].cantidad;
    }
    this.setState({indexProduct:index, selectProduct:true, cantidad:deleteProduct.cantidad, importe_pagado})
  }

  openCreditCardModal(event) {
    if (!this.state.dni) {
      this.setState({errClient:language("MsjSeleccioneCliente")})
    } else {
      if(event.target.value == 2) {
        this.setState({creditCardModal:false, indexCreditCard:this.state.index,tipo:event.target.value})
        CreditCard.All(this.state.User.access_token, "undefined", "dni", this.state.dni)
        .then(data=>{
            this.setState({creditCards: data  ? data : []})
          });
      } else{
        this.setState({tipo:event.target.value})
      }
    }
  }

  registerMercadoPago(index){
    swal(language("Factura") + `${this.state.facturas[index].num_factura} Conectandose con mercado pago `,{icon:"warning", button:false, timer:2000})
    Factura.openMercadoPago(this.state.facturas[index].id).then((data)=>{
      var a = document.createElement("a");
      a.href = data.url;
      a.click();
    }).catch((error)=>{
      swal(language("Factura") + `${this.state.facturas[index].num_factura} Error conentando revise la factura`,{icon:"warning", button:false, timer:2000})
    })
  }

  registerAfip(index){
    swal(language("Factura") + `${this.state.facturas[index].num_factura} Realizando registro `,{icon:"warning", button:false, timer:2000})
    Factura.registerAfip(this.state.facturas[index].id).then((data)=>{
      this.state.facturas[index] = data;
      this.setState({facturas:this.state.facturas})
    }).catch((error)=>{
      swal(language("Factura") + `${this.state.facturas[index].num_factura} Error registrando revise la factura`,{icon:"warning", button:false, timer:2000})
    })
  }

  updateData(e) {
    this.setState({
      checked:{},
      search:e.target.value !== '-1' ? 'codcliente':undefined,
      value:e.target.value !== '-1' ? this.state.clients[e.target.value]['\ufeffid'] : undefined,
      filterUser:e.target.value !== '-1' ? this.state.clients[e.target.value] : {email:'', nombre:''},
      dni:e.target.value !== '-1' ? this.state.clients[e.target.value].dni : '',
      codcliente:e.target.value !== '-1' ? this.state.clients[e.target.value]['\ufeffid'] : '',
      index:e.target.value !== '-1' ? e.target.value : 0,
      errClient:'',
      leyendaobservada:e.target.value !== '-1' ? this.state.clients[e.target.value].codtipodoc : '-1',
      observaciones: e.target.value !== '-1' ? this.state.typeFactura[this.state.typeFactura.map(data=>data.Desc).indexOf(this.state.clients[e.target.value].tipofactura)].Id : '-1' ,
      bon_intereses:e.target.value !== '-1' ? this.state.Ivas[this.state.Ivas.map(data=>data.Desc).indexOf('0%')].Id : '-1',
    },()=>{
        this.goToPage(1)

    })
  }

  handleChange(event) {
    let iva = this.state.Ivas[this.state.Ivas.map(data=>data.Id).indexOf(event.target.value)].Desc;
    iva = iva.replace("%","");
    let importe_pagado_old = this.state.importe_pagado_old;
    importe_pagado = importe_pagado_old + ((importe_pagado_old*iva)/100);

    this.setState({importe_pagado:importe_pagado, bon_intereses:event.target.value})
  }

  render(){
        return (
            <div style={{backgroundColor:'white'}}>
            {this.showChart()}
            { !this.state.loading ?
              <div>

              <ModalFactura isOpen={this.state.openReciboModal} toggle={this.toggleRecibos.bind(this)} factura={this.state.facturas[this.state.indexReciboModal]} />
              <Modal isOpen={this.state.createFacturaModal} toggle={this.toggleModal}
                className={this.props.className} size='lg'>
                <ModalHeader toggle={this.toggleModal}>{language("CrearFactura")}</ModalHeader>
                <ModalBody style={{'paddingLeft':'2%', 'paddingRight':'2%'}}>

                  <div>
                    <Row >
                      <Col>
                        <Row >
                          <Col md="7">
                            <FormGroup>
                              <div>
                                {language("Cliente")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.clients.map(data=>data.dni).indexOf(this.state.dni)} type="select" id="fecha" name="Fecha" placeholder=""
                                  onChange={(e) => this.updateData(e)} required={true}>
                                  <option value="-1">{language("MsjSeleccioneCliente")}</option>
                                  {
                                    this.state.clients.map((clients, i)=>{
                                      return(
                                        <option value={i}>{clients.nombre}</option>
                                      );
                                    })
                                  }
                                </Input>
                              </InputGroup>
                              <div className="errores">
                                {this.state.errClient}
                              </div>
                            </FormGroup>
                          </Col>
                          <Col md="5">
                            <FormGroup>
                              <div>
                                {language("NumeroFactura")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.num_factura} disabled type="number" id="n_factura" name="n_factura" placeholder="11" onChange={(event)=>this.setState({num_factura:event.target.value})} required={true}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="7">
                            <FormGroup>
                              <div>
                                {language("MsjTipoDoc")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.leyendaobservada} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({leyendaobservada:event.target.value})} required={true}>

                                  {
                                    this.state.typesDocument.map((type, i)=>{
                                      return(
                                        <option value={type.Id}>{type.Desc}</option>
                                      );
                                    })
                                  }
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md="5">
                            <FormGroup>
                              <div>
                                {language("MsjDoc")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.dni} type="text" id="Precio" name="Precio" placeholder="12" onChange={(event)=>this.setState({dni:event.target.value})} required={true}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="7">
                            <FormGroup>
                              <div>
                                {language("MsjTypeFactura")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.observaciones} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({observaciones:event.target.value})} required={true}>

                                  {
                                    this.state.typeFactura.map((typefactura, i)=>{
                                      return(
                                        <option value={typefactura.Id}>{typefactura.Desc}</option>
                                      );
                                    })
                                  }
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md="5">
                            <FormGroup>
                              <div>
                                {language("MsjIva")}
                              </div>
                              <InputGroup>
                                <Input value={this.state.bon_intereses} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.handleChange(event)} required={true}>

                                  {
                                    this.state.Ivas.map((iva, i)=>{
                                      return(
                                        <option value={iva.Id}>{iva.Desc}</option>
                                      );
                                    })
                                  }
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                      </Col>

                      <div>
      									<div style={{'width':'1px', 'height':'100%', 'backgroundColor':'#DDDDDD'}}/>
      								</div>

                      <Col>
                          <FormGroup>
                            <div>
                              {language("Fecha")}
                            </div>
                            <InputGroup>
                              <Input value={this.state.vencimiento} disabled type="date" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({vencimiento:event.target.value})} required={true}/>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                            <div>
                              {language("MetodoPago")}
                            </div>
                            <InputGroup>
                              <Input value={this.state.tipo} type="select" id="metodo_pago" name="MetodoPago"
                                onChange={
                                  (event)=>{
                                      this.openCreditCardModal(event);
                                  }
                                }
                                required={true}>
                                <option value={0}>{language("OpcionSelecciona")}</option>
                                <option value={'Efectivo'}>{language("Efectivo")}</option>
                                <option value={'CredictCard'}>{language("TarjetaCredito")}</option>

                              </Input>
                            </InputGroup>
                          </FormGroup>
                          {this.state.tipo == 2?
                            <FormGroup>
                              <div>
                                {language("TarjetaCredito")}
                              </div>
                              <InputGroup>
                                <Input type="select" id="metodo_pago" name="MetodoPago"
                                  onChange={
                                    (event)=>{
                                        this.setState({indexCreditCard:event.target.value})
                                    }
                                  }
                                  required={true}>
                                   <option value="-1">{language("OpcionSelecciona")}</option>
                                  {
                                    this.state.creditCards ?
                                    this.state.creditCards.map((creditCard, i)=>{
                                      return(
                                        <option value={i}>{creditCard.nombretarjeta + " " + creditCard.numerotarjeta}</option>
                                      );
                                    })
                                    : null
                                  }
                                </Input>
                              </InputGroup>
                            </FormGroup>
                          :
                            null }
                          {this.state.tipo == 3 ?
                           <Row>
                             <Col>
                               <FormGroup>
                                 <div>
                                   {language("FechaVencimiento")}
                                 </div>
                                 <InputGroup>
                                  <InputGroup>
                                   <Input type="date" id="fecha_vencimiento" name="FechaVencimiento" onChange={(event)=>this.setState({fecha_vencimiento:event.target.value})} required={true}/>
                                 </InputGroup>
                                 </InputGroup>
                               </FormGroup>
                             </Col>
                             <Col>
                               <FormGroup>
                                 <div>
                                   {language("MetodoPago")}
                                 </div>
                                 <InputGroup>
                                   <Input type="select" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.setState({forma:event.target.value})} required={true}>
                                     <option value={1}>{language("Efectivo")}</option>
                                     <option value={2}>{language("TarjetaCredito")}</option>
                                   </Input>
                                 </InputGroup>
                               </FormGroup>
                             </Col>
                           </Row>
                           :
                            null }
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <FormGroup>
                          <div>
                            {language("Producto")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.indexProduct} type="select" id="select_producto" name="AñadirProducto" onChange={(event)=>this.setState({indexProduct:event.target.value, selectProduct:true})} required={true}>
                              <option value={-1} >{language("MsjSeleccioneProducto")}</option>
                              {
                                this.state.facturas.map((factura, i)=>{

                                  if (Number(factura.estado) > 0) {
                                    return null;
                                  }
                                  for (let i = 0; i < this.state.productsSelect.length; i++)
                                    if(this.state.productsSelect[i]['\ufeffid'] === factura['\ufeffid'])
                                      return null;

                                  return(
                                    <option value={i}>{factura.descripcion}</option>
                                  );
                              })}
                            </Input>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>

                    {this.state.selectProduct ?
                      <Row>
                        <Col>
                        <FormGroup>
                          <div>
                            {language("Codigo")}
                          </div>
                          <InputGroup>
                            <Input disabled value={this.state.facturas[this.state.indexProduct].descripcion} style={{width:'100px'}} type="text" id="codigo" name="codigo" placeholder="1212" onChange={(event)=>this.setState({codigo_art:event.target.value})} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        </Col>
                        <Col>
                        <FormGroup>
                          <div>
                            {language("Precio")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.facturas[this.state.indexProduct].debe} disabled style={{width:'100px'}} type="text" id="Precio" name="Precio" placeholder="12" onChange={(event)=>this.setState({unitario:event.target.value})} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        </Col>


                        <Col>
                        <Button style={{'marginTop':'18px'}} onClick={this.addProducts.bind(this)} className="btn-round">
                          {language("AñadirProducto")}
                        </Button>
                        </Col>
                      </Row>
                    : null
                    }

                    <Table hover responsive>
                      <thead className="thead-default">
                        <tr>
                          <th className="justify-content-center text-center">{"Nº"}</th>
                          <th className="justify-content-center text-center">{language("Descripción")}</th>
                          <th className="justify-content-center text-center">{language("Cantidad")}</th>
                          <th className="justify-content-center text-center">{language("Precio")}</th>
                          <th className="justify-content-center text-center">{"Sub Total"}</th>

                          <th className="justify-content-center text-center">{language("Importe")}</th>
                        </tr>

                      </thead>
                      <tbody>
                      {
                        this.state.productsSelect.map((Products, i)=>{
                          return (
                            <tr key = {i}>
                              <td className="justify-content-center text-center">
                                {i+1}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.descripcion}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.cantidad}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.debe}
                              </td>
                              <td className="justify-content-center text-center">
                                {Number(Products.cantidad) * Number(Products.debe.replace(',','.')) }
                              </td>
                              <td className="justify-content-center text-center">
                                {Number(Products.cantidad) * Number(Products.debe.replace(',','.')) }
                              </td>

                              <td className="justify-content-center ">

                                <tr><i className="now-ui-icons shopping_credit-card" onClick={()=>this.deleteProduct(i)}>{language("BotonEliminar")}</i></tr>
                              </td>
                            </tr>
                          );
                        })
                      }
                      <td className="justify-content-center text-center">

                      </td>
                       <td className="justify-content-center text-center">

                      </td>
                      <td className="justify-content-center text-center">

                      </td>
                      <td className="justify-content-center text-center">

                      </td>
                      <td className="justify-content-center text-center">
                       {"Total"}
                      </td>
                      <td className="justify-content-center text-center">
                        {this.state.importe_pagado}
                      </td>
                      </tbody>
                    </Table>

                    <FormGroup>
                      <div>
                        {language("ImportePagar")}
                      </div>
                      <InputGroup>
                        <Input value={this.state.pagado
                        } onChange={(event)=>this.setState({pagado:event.target.value})} type="number" id="register_confirm_password" name="Password" placeholder="12" required={true}/>
                      </InputGroup>
                    </FormGroup>
                  </div>

                </ModalBody>
                <ModalFooter className='justify-content-right text-right'>
                  <Button disabled={this.state.disabled} color="primaryBlue" onClick={this.state.updateFactura ? this.updateFactura.bind(this) :this.createFactura.bind(this)}>{this.state.updateFactura ? language('BotonEditar') : language("BotonCrear")}</Button>
                </ModalFooter>

              </Modal>
              <div style={{'paddingLeft':'2%', 'paddingRight':'2%', 'paddingTop':'2%', 'backgroundColor':'white'}}>
                <Row >
                  <Col style={{borderColor:'#2ca8ff', margin:10, borderWidth:4, borderStyle:'solid', borderRadius:8}}>
                    <Row>
                    <FormGroup>
                      <div>
                        {language("Fecha")}
                      </div>
                      <InputGroup>
                        <Input disabled value={this.state.vencimiento} style={{width:'300px'}} type="date" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.getData(this.state.page,event.target.value !== '-1' ? 'created_at':undefined , event.target.value !== '-1' ? event.target.value : undefined)} required={true}/>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup style={{'marginLeft':'1%'}}>
                      <div>
                        {language("Clientes")}
                      </div>
                      <InputGroup>
                        <Input value={this.state.clients.map(data=>data.dni).indexOf(this.state.dni)} style={{width:'200px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={this.updateData} required={true}>
                           <option value="-1">{language("MsjSeleccioneCliente")}</option>
                            {
                              this.state.clients.map((client, i)=>{
                                return(
                                  <option value={i}>{client.nombre}</option>
                                );
                              })
                            }
                        </Input>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <div style={{'color':'white'}}>
                          {language("CreateEmail")}
                        </div>
                        <InputGroup>
                          <Input disabled value={this.state.filterUser.email} style={{width:'200px'}} type="text" id="email" name="Email" placeholder="username" onChange={(event)=>this.setState({Email:event.target.value})} required={true}/>
                        </InputGroup>
                      </FormGroup>

                    </Row>
                  </Col>
                  <Col>
                    <Row style={{borderColor:'#2ca8ff', borderRightWidth:4, margin: 10 ,borderLeftWidth:4, borderTopWidth:4, borderBottomWidth:4 , borderStyle:'solid', borderRadius:8}}>
                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div>
                          {language("Servidor")}
                        </div>
                        <InputGroup>
                          <Input style={{width:'150px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({Title:event.target.value})} required={true}>
                             <option >{"RIO GRANDE"}</option>
                             <option>{"Segundo servidor"}</option>
                          </Input>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div>
                          {"AP"}
                        </div>
                          <InputGroup>
                            <Input style={{width:'150px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({Title:event.target.value})} required={true}>
                               <option>{"AUSTRAL SUR"}</option>
                               <option>{"Segundo AP"}</option>
                            </Input>
                          </InputGroup>
                      </FormGroup>
                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div style={{'color':'white'}}>
                          {"prueba"}
                        </div>
                          <InputGroup>
                            <Input style={{width:'150px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({Title:event.target.value})} required={true}>
                               <option>{"Activos"}</option>
                               <option>{"Inactivos"}</option>
                            </Input>
                          </InputGroup>
                      </FormGroup>
                    </Row>
                    <Row style={{borderColor:'#2ca8ff',margin:10, borderRightWidth:4,borderLeftWidth:4, borderTopWidth:4, borderBottomWidth:4 , borderStyle:'solid', borderRadius:8}}>

                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div>
                          {"IP"}
                        </div>
                        <InputGroup>
                          <Input style={{width:'100px'}} type="number" id="email" name="Email" placeholder="10.10.10.10" onChange={(event)=>this.setState({Email:event.target.value})} required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div>
                          {language("Tolerancia")}
                        </div>
                        <InputGroup>
                          <Input style={{width:'100px'}} type="number" id="email" name="Email" placeholder="0" onChange={(event)=>this.setState({Email:event.target.value})} required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup style={{'marginLeft':'1%'}}>
                        <div>
                          {language("Adeuda")}
                        </div>
                        <InputGroup>
                          <Input style={{width:'100px'}} type="number" id="email" name="Email" placeholder="0" onChange={(event)=>this.setState({Email:event.target.value})} required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <div style={{'marginLeft':'1%', 'marginTop':'1%'}}>
                        <Radio
                            label={language("OpcionEmpresa")}
                            inputProps={{defaultChecked:false,name:"radio",value:"checked"}}
                        />
                      </div>
                    </Row>
                  </Col>
                </Row>

              </div>

                <Card>

                  <CardBody>
                    <div className="show-scroll">
                      <Table responsive hover>
                        <thead className="text-primary">
                            <th className="justify-content-center text-center">{""}</th>
                            <th className="justify-content-center text-center">{"Nº"}</th>
                            <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                            <th className="justify-content-center text-center">{language("Vencimiento")}</th>
                            <th className="justify-content-center text-center">{language("Debe")}</th>
                            <th className="justify-content-center text-center">{language("Mora")}</th>
                            <th className="justify-content-center text-center">{language("Intereses")}</th>
                            <th className="justify-content-center text-center">{language("Importe")}</th>
                            <th className="justify-content-center text-center">{"Total"}</th>
                            <th className="justify-content-center text-center">{language("Abonado")}</th>
                            <th className="justify-content-center text-center">{language("Moneda")}</th>
                            <th className="justify-content-center text-center">{language("Situacion")}</th>
                            <th className="justify-content-center text-center">{language("Observación")}</th>
                            <th className="justify-content-center text-center">{language("Opciones")}</th>
                        </thead>
                        <tbody>
                            {
                              this.state.facturas.map((Facturas,i)=>{
                                return (
                                  <tr >
                                    <td className="justify-content-center text-center">
                                      <Checkbox
                                        label={''}
                                        inputProps={{ defaultChecked: this.state.checked[Facturas['\ufeffid']], onChange:event=>{

                                          if (this.state.checked[Facturas['\ufeffid']]) {
                                            this.deleteProduct(i);
                                            return;
                                          };
                                          this.setState({indexProduct:i, selectProduct:true}, ()=>{this.addProducts()})}
                                        }}

                                      />
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.num_factura}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.descripcion}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.vencimiento.split(' ')[0]}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.debe}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.mora}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.bon_intereses}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.importe_pagado}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.importe_pagado}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.pagado}
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.forma === "1" ? language("Efectivo") : (Facturas.tipo === "2" ? language("TarjetaCredito") : language("PorCuotas")) }
                                    </td>
                                    <td className="justify-content-center text-center">
                                      {Facturas.estado === '0' ? 'No pagado' : (Facturas.estado === '1' ? 'Pagado' :'Pagado')}
                                    </td>

                                    <td className="justify-content-center text-center">{""}</td>
                                    <td className="justify-content-center ">




                                      {
                                        Facturas.cae !== '' && Facturas.cae !== '-'  && <tr>
                                        <a target='_black' href={"/pdf/generar?accion=ver&id="+ Facturas.cae}><i className="now-ui-icons files_single-copy-04" >{language("VerFactura")}</i></a>
                                      </tr>
                                      }

                                      <tr onClick={()=>this.toggleRecibos(i)} ><i  className="now-ui-icons shopping_credit-card">{language("VerRecibo")}</i></tr>
                                    </td>
                                  </tr>
                                );
                              })
                            }
                        </tbody>
                      </Table>
                    </div>
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                      </PaginationItem>
                      {this.state.total.map((data, i)=>{
                        if ((this.state.total.length > 15 && i < this.state.page +15 && i >= this.state.page )|| this.state.total.length <= 15) {
                            return(
                              <PaginationItem>
                                <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                                  {i+1}
                                </PaginationLink>
                              </PaginationItem>
                            );
                        }

                      })}


                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                      </PaginationItem>
                    </Pagination>
                  </CardBody>
                </Card>




              <div style={{paddingBottom:'1%', paddingRight:'1%'}} className="fixed-right justify-content-right text-right">
                <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={this.toggleModal}>
                  <i className="now-ui-icons ui-1_simple-add"></i>
                </Button>
              </div>
              </div>
              :

                  <div className="flex-center" style={{height:'288px'}}>
                    <Loader
                      type="TailSpin"
                      color="#2ca8FF"
                      height="50"
                      width="50"
                    />

                  </div>
                }

            </div>
        );
    }
}


/*

<td className="justify-content-center ">
                                      {
                                        (!Facturas.cae && Boolean(Facturas.estado)) &&
                                        <div>
                                          <tr>
                                            <i style={{color:'#f96332'}}className="now-ui-icons files_single-copy-04" onClick={()=>this.registerAfip(i)}>{language("BotonDarAlta")}</i>
                                          </tr>

                                        </div>
                                      }

                                      {
                                        !Boolean(Facturas.estado) &&
                                        <div>
                                          <tr>
                                            <i style={{color:'#f96332'}}className="now-ui-icons files_single-copy-04" onClick={()=>this.registerMercadoPago(i)}>{language("MensajeMercado")}</i>
                                          </tr>
                                          <tr>
                                            <i className="now-ui-icons files_single-copy-04" onClick={()=>this.openUpdate(i)}>{language("BotonEditar")}</i>
                                          </tr>
                                        </div>
                                      }


                                      <tr>
                                        <a target='_black' href={"/pdf/generar?accion=ver&id="+ Facturas.id}><i className="now-ui-icons files_single-copy-04" >{language("VerFactura")}</i></a>
                                      </tr>
                                      <tr onClick={()=>this.toggleRecibos(i)} ><i  className="now-ui-icons shopping_credit-card">{language("VerRecibo")}</i></tr>
                                    </td>
     <div style={{'paddingLeft':'2%', 'paddingRight':'2%', 'paddingTop':'2%', 'backgroundColor':'white'}}>
                <Row>
                  <Button onClick={this.encontruccion.bind(this)} >
                      {language("BotonPagoTarjeta")}
                  </Button>
                  <Button onClick={this.encontruccion.bind(this)} style={{'marginLeft':'1%'}}>
                      {language("IngresarDebitos")}
                  </Button>
                  <Button  onClick={this.encontruccion.bind(this)} style={{'marginLeft':'1%'}}>
                      {language("BotonCargarCuotas")}
                  </Button>
                  <Button onClick={this.encontruccion.bind(this)} style={{'marginLeft':'1%'}}>
                      {language("EnviarCorreo")}
                  </Button>
                  <Button onClick={this.encontruccion.bind(this)} style={{'marginLeft':'1%'}}>
                      {language("ImprimirResumen")}
                  </Button>
                  <div style={{'marginLeft':'1%'}} >
                    <Checkbox
                      label={language("MsjNoEnviarEmail")}
                      inputProps={{defaultChecked: true}}
                    />
                  </div>
                </Row>
              </div>
 */
export default Facturas;
