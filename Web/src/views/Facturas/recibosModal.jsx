import React from 'react';

import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';


import {
    Button,  Row, Col, Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card, 
    CardHeader, 
    CardBody, 
    CardTitle,
    FormFeedback,
    Label,
    Pagination,
    PaginationItem,
    PaginationLink,
    FormText
} from 'reactstrap';


import {
    PanelHeader,
    Checkbox,
    Radio
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import Recibo from '../../api/services/recibo';
import Cliente from '../../api/services/client';
import Factura from '../../api/services/factura';

class Recibos extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  recibos:[],
                  facturas:[],
                  allFacturas:[],
                  factura:{},
                  clientes:[],
                  total:[],
                  client_id:-1,
                  User:Cookies.load('userId'),
                  administrador:'0',
                  updateBirdcall:false,
                  nombre:'',
                  err:false,
                  seeRecibos:false,
                  page:1,
                  search:undefined,
                  value:undefined,
                  errores:{errNombre:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}
                 };

    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    
  }

  searchData(search, value){
    if(this.state.search === search && this.state.value===value){
      this.setState({page:1, search:undefined, value:undefined})
      this.getData(1);
      return;
    }
    this.setState({page:1, search, value})
    this.getData(1,search, value );
  }

  componentWillReceiveProps(props) {
      console.log(props)
      if(props.factura)
    this.getData(this.state.page, props.factura ? "facturas_id":undefined, props.factura ? props.factura.id:undefined);
  }


  getData(page, search, value){
    Recibo.All(this.state.User.access_token, page, search, value)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)  
          }
          console.log(data)
          this.setState({recibos: data  ? data.data : [], total})
        });
  
  }
  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1, this.state.search, this.state.value);
    this.setState({page:this.state.page+1});
    
  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1, this.state.search, this.state.value);
    this.setState({page:this.state.page-1});
    
  }
 
  goToPage(index) {
    this.getData(index, this.state.search, this.state.value);
    this.setState({page:index});
    
  }


  render(){
    
        return (     
            <Modal className="boton-volver" size='lg' isOpen={this.props.isOpen} toggle={this.props.toggle} >
                <ModalHeader  >
                    <table>
                        <tr>
                        <td>{ this.state.updateBirdcall ? language('BotonEditar') : language("BotonRegistrar")}</td>
                        </tr>
                    </table>
                </ModalHeader>
                    <Card>
                        <CardBody>
                            <Table hover responsive>
                            <thead className="text-primary">
                                <tr>
                                <th className="justify-content-center text-center">{language("Factura")}</th>
                                <th className="justify-content-center text-center">{language("DetalleArticulo")}</th> 
                                <th className="justify-content-center text-center">{language("Intereses")}</th> 
                                <th className="justify-content-center text-center">{language("Importe")}</th> 
                                <th className="justify-content-center text-center">{language("Recibo")}</th>
                                <th className="justify-content-center text-center">{language("Mora")}</th> 
                                <th className="justify-content-center text-center">{language("Opciones")}</th> 
                                </tr>
                            </thead>
                            <tbody>
                                {
                                this.state.recibos.map((Recibos, i)=>{
                                    return (
                                    <tr key = {i}>
                                        <td className="justify-content-center text-center">
                                        {Recibos.facturas_id}
                                        </td>
                                        <td className="justify-content-center text-center">
                                        {Recibos.descripcion}
                                        </td>
                                        <td className="justify-content-center text-center">
                                        {Recibos.intereses}
                                        </td>
                                        <td className="justify-content-center text-center">
                                        {Recibos.importe}
                                        </td>
                                        <td className="justify-content-center text-center">
                                        {Recibos.id}
                                        </td>
                                        <td className="justify-content-center text-center">
                                        {"20"}
                                        </td>
                                        <td className="justify-content-center ">
                                        <tr>
                                            <a target='_black' href={"/pdf/generarrecibo?accion=get&id="+ Recibos.id}><i className="now-ui-icons files_single-copy-04" >{language("VerRecibo")}</i></a>
                                        </tr>
                                        </td>
                                    </tr>
                                    );
                                    })
                                }
                            </tbody>
                            <Pagination aria-label="Page navigation example">
                                <PaginationItem>
                                <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                                </PaginationItem>
                                {this.state.total.map((data, i)=>{
                                if ((this.state.total.length > 15 && i < this.state.page +15 && i >= this.state.page )|| this.state.total.length <= 15) {
                                      return(
                                        <PaginationItem>
                                          <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                                            {i+1}
                                          </PaginationLink>
                                        </PaginationItem>
                                      );
                                  }
                                })}
                                <PaginationItem>
                                <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                                </PaginationItem>
                            </Pagination>
                            </Table>
                        </CardBody>
                    </Card>
               
            </Modal>   
        );
    }
}

export default Recibos;
