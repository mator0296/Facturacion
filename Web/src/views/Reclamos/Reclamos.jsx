import React from 'react';

/*
import {
    UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Table,
} from 'reactstrap';
*/

import {
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,  Row,  Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card, CardBody, CardHeader
} from 'reactstrap';

/*
// react plugin used to create charts
import { Line, Bar } from 'react-chartjs-2';
*/

// function that returns a color based on an interval of numbers

import {
    PanelHeader
} from '../../components';

//import Api from '../../api/Api/Api'
import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import Cliente from '../../api/services/client';
import Factura from '../../api/services/factura';
import Reclamo from '../../api/services/reclamos';

class Reclamos extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  reclamos:[],
                  loading:false,
                  User:Cookies.load('userId'),
                  facturas:[],
                  clientes:[],
                  facturas_id:-1,
                  personales_id:-1,
                  indexToEdit:-1,
                  page:1,
                  descripcion:"",
                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.seeFacturas = this.seeFacturas.bind(this);
    this.getData(this.state.page);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    this.updateReclamo = this.updateReclamo.bind(this)
    Cliente.All(this.state.User.access_token)
      .then(data=>this.setState({clientes: data ? data : []}));

  }

  getData(page){
    Reclamo.All(this.state.User.access_token,page)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)  
          }
          
          this.setState({reclamos: data  ? data.data : [], total})
        });
  
  }

  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1);
    this.setState({page:this.state.page+1});
    
  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1);
    this.setState({page:this.state.page-1});
    
  }
 
  goToPage(index) {
    this.getData(index);
    this.setState({page:index});
    
  }

  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {
    this.setState({
      toggle: !this.state.toggle
    });
  }

  createReclamo(){
    let bandera = true;
    let errores = {errores:{errNombre:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}};

    if (this.state.descripcion === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.personales_id === -1) {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.facturas_id === -1) {
      bandera = false;
      this.setState({err:true})
    }

    if (bandera) {
      this.setState({loading:true, disabled:true});
      Reclamo.Create(this.state.User.access_token,this.state)
        .then((data)=>{
          this.setState({loading: true, disabled: true})
          if (data.error) {
            swal('Oops...',data.error,'warning')
            this.setState({loading: false, disabled: false})
          } else {
            console.log(data);
            this.state.reclamos.push(data);
            this.setState({toggle:false, loading: false, disabled: false})
            swal(language("Reclamo") + ` ${this.state.descripcion} ` + language("MsjCreadoExito"),{icon:"success", button:false, timer:2000})
          }

        })
    }
    this.setState({...this.state, errores:errores});
  }

  deleteListener(index) {
    swal({
      title: language("MsjEliminarReclamo") + ` ${this.state.reclamos[index].reclamo.id} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      buttons:
      {
        exit:{
          text:language("BotonCancelar")
        },
        delete:{
          text:language("BotonEliminar"),
          className:'btn-danger'
        },
      }
    }).then(willDelete=>{
      if(willDelete!== 'delete') return;
      Reclamo.Delete(this.state.User.access_token, this.state.reclamos[index].reclamo.id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjEliminadoReclamo") +` ${this.state.reclamos[index].reclamo.id}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({reclamos:this.state.reclamos.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.reclamos[index].reclamo.id}`, 'warning')
        })

    })
  }


  openUpdate(index){
    Factura.FacturaForClient(this.state.User.access_token, this.state.reclamos[index].reclamo.personales_id)
        .then(data=>this.setState({facturas: data ? data : {}}));
    this.setState({...this.state.reclamos[index].reclamo, toggle:true, updateReclamo:true, indexToEdit:index})
  }

  updateReclamo(){
    this.setState({loading:true, disabled:true});
     Reclamo.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        this.setState({loading: true, disabled: true})
        if (data.error) {
          swal('Oops...',data.error,'warning')
          this.setState({loading: false, disabled: false})
        } else {
          this.state.reclamos[this.state.indexToEdit] = data;
          swal(language("Reclamo") + `${this.state.id}` + language("Editado"),{icon:"success", button:false, timer:2000})
          this.setState({toggle:false, loading: false, disabled: false, updateReclamo:false})
         }
      })
    }

    seeFacturas(event) {
      Factura.FacturaForClient(this.state.User.access_token, event.target.value)
        .then(data=>this.setState({facturas: data ? data : []}));
      this.state.personales_id = event.target.value;
      console.log("cliente", this.state.facturas_personales_id)
    }

    saveFactura(event) {
      console.log(event.target.value)
      Factura.Get(this.state.User.access_token, event.target.value)
        .then(data=>this.setState({factura: data ? data : {}}));
      this.state.facturas_id = event.target.value;
      
    }

    render(){
        console.log(this.state)
        return (
            <div>

              {this.showChart()}

              { this.state.reclamos[0] != null ?

              <Card>

              <CardBody>

                <Table hover responsive>
                  <thead className="text-primary">
                    <tr>
                      <th className="justify-content-center text-center">{"Nº"}</th>
                      <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                      <th className="justify-content-center text-center">{language("Cliente")}</th>
                      <th className="justify-content-center text-center">{language("Factura")}</th>
                      <th className="justify-content-center text-center">{language("Opciones")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.reclamos.map((reclamos, i)=>{
                      return(
                        <tr key = {i}>
                          <td className="justify-content-center text-center">{reclamos.reclamo.id}</td>
                          <td className="justify-content-center text-center">{reclamos.reclamo.descripcion}</td>
                          <td className="justify-content-center text-center">{reclamos.nombre}</td>
                          <td className="justify-content-center text-center">{reclamos.num_factura}</td>
                          <td className="justify-content-center ">
                            <tr>
                              <i className="now-ui-icons files_single-copy-04" onClick={()=>this.openUpdate(i)}>{language("BotonEditar")}</i>
                            </tr>
                            <tr>
                              <i className="now-ui-icons shopping_credit-card" onClick={()=>this.deleteListener(i)}>{language("BotonEliminar")}</i>
                            </tr>
                          </td>
                        </tr>
                      );

                    })}
                  </tbody>
                </Table>
                <Pagination aria-label="Page navigation example">
                  <PaginationItem>
                    <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                  </PaginationItem>
                  {this.state.total.map((data, i)=>{
                    return(
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                          {i+1}
                        </PaginationLink>
                      </PaginationItem>
                    );
                  })}
                  <PaginationItem>
                    <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                  </PaginationItem>
                </Pagination>
              
                </CardBody>
                </Card>

              :

              <div className="content flex-center animated animatedFadeInUp fadeInUp">
                  <h2>{language("MsjNoReclamo")}</h2>
              </div>

              }

              <Modal className="boton-volver" style={{}} size='lg' isOpen={this.state.toggle} toggle={()=>{ this.setState({toggle:false})}} >
                <ModalHeader  >
                    <table>
                      <tr>
                        <td>{ this.state.updateReclamo ? language('BotonEditar') : language("BotonRegistrar")}</td>
                      </tr>
                    </table>
                  </ModalHeader>

                  { !this.state.loading ?

                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>
                          <FormGroup>
                            <div>
                              {language("DetalleArticulo")+" *"}
                            </div>
                            <InputGroup>
                              <Input value={this.state.descripcion} onChange={(event)=>this.setState({descripcion:event.target.value})} type="text" id="register_name" name="Name" placeholder={this.state.reclamos[this.state.indexToEdit] != null ? this.state.reclamos[this.state.indexToEdit].reclamo.descripcion : language("DetalleArticulo")} required={true}/>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                            <div>
                              {language("Clientes")+" *"}
                            </div>
                            <InputGroup>

                              <Input value={this.state.personales_id} style={{width:'200px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>{this.seeFacturas(event)}} required={true}>
                                <option value="-1">{language("OpcionSelecciona")}</option>
                                {
                                  this.state.clientes.map((clientes, i)=>{
                                    return(
                                      <option value={clientes.dni}>{clientes.nombre}</option>
                                    );
                                  })
                                }
                              </Input>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                            <div>
                              {language("Facturas")+" *"}
                            </div>
                            <InputGroup>
                              <Input value={this.state.facturas_id} style={{width:'200px'}} type="select" id="factura_id" name="Factura" onChange={(event)=>{this.saveFactura(event)}} required={true}>
                                <option value="-1">{language("OpcionSelecciona")}</option>
                                {
                                  this.state.facturas.map((factura, i)=>{
                                    return(
                                      <option value={factura.id_factura}>{factura.num_factura + " " + factura.descripcion}</option>
                                    );
                                  })
                                }
                              </Input>
                            </InputGroup>
                          </FormGroup>
                          { this.state.err ?
                           <div className="errores">{language("ErrorCampo")}</div>
                           : null
                          }
                        
                      </div>
                    :
                      <div className="flex-center" style={{height:'450px'}}>
                        <Loader
                           type="TailSpin"
                           color="#2ca8FF"
                           height="50"
                           width="50"
                        />
                      </div>
                    }
                  <ModalFooter >
                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateReclamo ? this.updateReclamo : this.createReclamo.bind(this)}>{ this.state.updateReclamo ? language('BotonEditar') : language("BotonRegistrar")}</Button>
                  </ModalFooter>
                </Modal>

              <div style={{paddingBottom:'2%', paddingRight:'2%'}} className="fixed-right justify-content-right text-right">
                <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={this.toggleModal}>
                  <i className="now-ui-icons ui-1_simple-add"></i>
                </Button>
              </div>

            </div>
        );
    }
}

export default Reclamos;
