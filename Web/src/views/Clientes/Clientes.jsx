import React from 'react';

import {
    Button,  Row, Col, Modal,  ModalHeader,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card,
    CardBody,
    Pagination,
    PaginationItem,
    PaginationLink
} from 'reactstrap';


import {
    PanelHeader
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import CreditCard from './creditCard';
import Factura from '../../api/services/factura';
import Client from '../../api/services/client';

class Clientes extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  clients:[],
                  User:Cookies.load('userId'),
                  administrador:'0',
                  updateClient:false,
                  condicioniva:"",
                  targetValue:-1,
                  tipodoc:"",
                  codtipodoc:0,
                  tipofactura:"",
                  nombre:'',
                  typesDocument:[],
                  Ivas:[],
                  typeFactura:[],
                  password:'',
                  zona:"",
                  fecha_nac:"",
                  err:false,
                  loading:false,
                  disabled:false,
                  page:1,
                  errores:{errNombre:'', errPassword:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: '', errIva:'', errTipoDoc:'', errTipoFactura:''},
                  pageCreditCard:1,
                  creditCards:[],
                  totalCredictCard:[],

                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.updateClient = this.updateClient.bind(this);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    this.getData(this.state.page);
     Factura.GetDataNewFactura().then((data)=>{
      this.setState({ typesDocument:data.typeDocument, Ivas:data.iva, typeFactura:data.typeFactura});
    })
  }

  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {

    Factura.GetDataNewFactura().then((data)=>{
      this.setState({ typesDocument:data.typeDocument, Ivas:data.iva, typeFactura:data.typeFactura});
    })

    this.setState({
      createUserModal: !this.state.createUserModal,
      condicioniva:"",
      targetValue:-1,
      tipodoc:"",
      codtipodoc:0,
      tipofactura:"",
      nombre:'',
      dni:'',
      email:'',
      password:'',
      ciudad:'',
      email2:'',
      ap:'',
      telefono:'',
      codigo_postal:'',
      Barrio:'',
      fecha_nac:'',
      domicilio:'',
      cod_barrio:'',
      provincia:'',
      zona:'',
      updateClient:false,
      loading:false
    });
  }

  createClient(){
    let bandera = true;
    let errores = {errores:{errNombre:'', errPassword:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: '', errIva:'', errTipoDoc:'', errTipoFactura:''}};

    console.log(this.state)

    if (this.state.nombre === "" ) {
      bandera = false;
      errores.errores.errNombre = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.password === "" ) {
      bandera = false;
      errores.errores.errPassword = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.dni === "") {
      bandera = false;
      errores.errores.errDni = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.email === "") {
      bandera = false;
      errores.errores.errEmail = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.ciudad === "") {
      bandera = false;
      errores.errores.errCiudad = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.ap === "") {
      bandera = false;
      errores.errores.errAp = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.telefono === "") {
      bandera = false;
      errores.errores.errTelefono = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.zona === "") {
      bandera = false;
      errores.errores.errZona = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.codigo_postal === "") {
      bandera = false;
      errores.errores.errCodigoPostal = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.Barrio === "") {
      bandera = false;
      errores.errores.errBarrio = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.fecha_nac === "") {
      bandera = false;
      errores.errores.errFechaNac = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.domicilio === "") {
      bandera = false;
      errores.errores.errDomicilio = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.condicioniva === "") {
      bandera = false;
      errores.errores.errIva = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.tipofactura === "") {
      bandera = false;
      errores.errores.errTipoFactura = language("ErrorCampo");
      this.state.err = true;
    }

    if (this.state.tipodoc === "") {
      bandera = false;
      errores.errores.errTipoDoc = language("ErrorCampo");
      this.state.err = true;
    }

    if (bandera) {
      this.setState({loading:true, disabled:true})
       Client.Create(this.state.User.access_token,this.state)
        .then((data)=>{

          if (data.error) {
            swal('Oops...',data.error,'warning')
            this.setState({loading: false, disabled: false})
          } else {
            console.log(data);
            this.state.clients.push(data);
            this.setState({createUserModal:false, loading: false, disabled: false})
            swal(language("Cliente") + ` ${this.state.nombre} ` + language("MsjCreadoExito"),{icon:"success", button:false, timer:2000})
          }

        })
    }
    this.setState({...this.state, errores:errores});

  }

  deleteListener(index) {
    swal({
      title: language("MsjEliminarCliente") + ` ${this.state.clients[index].nombre} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      buttons:
      {
        exit:{
          text:language("BotonCancelar")
        },
        delete:{
          text:language("BotonEliminar"),
          className:'btn-danger'
        },
      }
    }).then(willDelete=>{
      if(willDelete!== 'delete') return;
      Client.Delete(this.state.User.access_token, this.state.clients[index].id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjClienteEliminado") +` ${this.state.clients[index].nombre}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({clients:this.state.clients.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.clients[index].nombre}`, 'warning')
        })

    })


  }


  openUpdate(index){
    Factura.GetDataNewFactura().then((data)=>{
      this.setState({ typesDocument:data.typeDocument, Ivas:data.iva, typeFactura:data.typeFactura, });
    })
    this.setState({...this.state.clients[index], createUserModal:true, updateClient:true, indexToEdit:index, targetValue:this.state.typesDocument.map(data=>data.Id).indexOf(this.state.clients[index].codtipodoc) })
  }

  updateClient(){
     Client.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        this.setState({loading: true, disabled: true})
        if (data.error) {
          swal('Oops...',data.error,'warning')
          this.setState({loading: false, disabled: false})
        } else {
          this.state.clients[this.state.indexToEdit] = data;
          swal(language("Cliente") + `${this.state.nombre}` + language("Editado"),{icon:"success", button:false, timer:2000})
          this.setState({createUserModal:false, loading: false, disabled: false, updateClient:false})
        }
      })

  }

  getData(page){
    Client.All(this.state.User.access_token, page)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)
          }
          console.log(data)
          this.setState({clients: data  ? data.data : [], total})
        });

  }

  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1);
    this.setState({page:this.state.page+1});

  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1);
    this.setState({page:this.state.page-1});

  }

  goToPage(index) {
    this.getData(index);
    this.setState({page:index});

  }

  togleCreditCard(){
    this.setState({creditCardModal:!this.state.creditCardModal})
  }
  openCreditCardModal(index) {
    this.setState({creditCardModal:true, indexCreditCard:index})
  }

  render(){
        return (
            <div>

              {this.showChart()}

              { this.state.clients[0] != null ?

                <Card>

                  <CardBody>

                    <div className="show-scroll">
                      <Table hover responsive className="full-width">
                        <thead className="text-primary">
                          <tr>
                            <th className="justify-content-center text-center">{language("CreateName")}</th>
                            <th className="justify-content-center text-center">{language("CreateApellido")}</th>
                            <th className="justify-content-center text-center">{language("CreateEmail")}</th>
                            <th className="justify-content-center text-center">{language("CreateEmailSecundario")}</th>
                            <th className="justify-content-center text-center">{language("NumeroTlf")}</th>
                            <th className="justify-content-center text-center">{language("Domicilio")}</th>
                            <th className="justify-content-center text-center">{language("CreateFechaNacimiento")}</th>
                            <th className="justify-content-center text-center">{language("Opciones")}</th>
                          </tr>
                        </thead>
                        <tbody>
                          { this.state.clients.map((clients, i)=>{
                            return(
                              <tr key = {i}>
                                <td className="justify-content-center text-center">
                                  {clients.nombre}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.ap}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.email}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.email2}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.telefono}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.domicilio}
                                </td>
                                <td className="justify-content-center text-center">
                                  {clients.fecha_nac}
                                </td>
                                <td className="justify-content-center ">
                                  <tr>
                                    <i className="now-ui-icons files_single-copy-04" onClick={()=>this.openUpdate(i)}>{language("BotonEditar")}</i>
                                  </tr>
                                  <tr>
                                    <i className="now-ui-icons shopping_credit-card" onClick={()=>this.deleteListener(i)}>{language("BotonEliminar")}</i>
                                  </tr>
                                  <tr>
                                    <i className="now-ui-icons shopping_credit-card" onClick={()=>this.openCreditCardModal(i)}>{language("TarjetaCredito")}</i>
                                  </tr>
                                </td>
                              </tr>
                            );

                          })}
                        </tbody>
                      </Table>
                    </div>
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                      </PaginationItem>
                      {this.state.total.map((data, i)=>{
                           return(
                              <PaginationItem>
                                <PaginationLink style={{backgroundColor:this.state.pageFactura === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPageFactura(i+1)}>
                                  {i+1}
                                </PaginationLink>
                              </PaginationItem>
                            );
                      })}
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                      </PaginationItem>
                    </Pagination>
                  </CardBody>
                </Card>

              :

              <div className="content flex-center animated animatedFadeInUp fadeInUp">
                  <h2>{language("MsjCrearClientes")}</h2>
              </div>

              }

              <div style={{paddingBottom:'1%', paddingRight:'1%'}} className="fixed-right justify-content-right text-right">
                <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={this.toggleModal}>
                  <i className="now-ui-icons ui-1_simple-add"></i>
                </Button>
              </div>
              <CreditCard isOpen={this.state.creditCardModal} user={this.state.clients[this.state.indexCreditCard]} toggle={this.togleCreditCard.bind(this)}/>
              <Modal className="boton-volver" style={{}} size='lg' isOpen={this.state.createUserModal} toggle={()=>{ this.setState({createUserModal:false})}} >
                <ModalHeader  >
                    <table>
                      <tr>
                        <td>{ this.state.updateClient ? language('BotonEditar') : language("BotonRegistrar")}</td>
                      </tr>
                    </table>
                  </ModalHeader>

                  { !this.state.loading ?

                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>
                      <Row>
                        <Col>
                          <FormGroup>
                            <div>
                              {language("CreateName")+" *"}
                            </div>
                            <InputGroup>
                              <Input value={this.state.nombre} onChange={(event)=>this.setState({nombre:event.target.value})} type="text" id="register_name" name="Name" placeholder={language("TuNombre")} required={true}/>
                            </InputGroup>
                            <div className="errores">{this.state.errores.errNombre}</div>
                          </FormGroup>
                          <FormGroup>
                            <div>
                              {language("MsjTipoDoc") + " *"}
                            </div>
                            <InputGroup>

                              <Input value={this.state.targetValue} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({targetValue:event.target.value, tipodoc:this.state.typesDocument[event.target.value].Desc, codtipodoc:this.state.typesDocument[event.target.value].Id})} required={true}>
                                <option value="-1">{language("OpcionSelecciona")}</option>
                                {
                                  this.state.typesDocument.map((type, i)=>{
                                    return(
                                      <option value={i}>{type.Desc}</option>
                                    );
                                  })
                                }
                              </Input>
                              <div className="errores">{this.state.errores.errTipoDoc}</div>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                          <div>
                            {language("MsjDoc") + " *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.dni} onChange={(event)=>this.setState({dni:event.target.value})} type="number" id="dni" name="DNI" placeholder={language("TuDni")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CreateEmail") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.email} onChange={(event)=>this.setState({email:event.target.value})} type="email" id="register_email" name="Email" placeholder={language("TuCorreo")} required={true}/>
                          </InputGroup>
                          <div className="errores">{this.state.errores.errEmail}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CreatePassword")+" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.password} onChange={(event)=>this.setState({password:event.target.value})} type="password" id="register_password" name="Password" placeholder={language("Tu") + " " +language("CreatePassword")}  required={true}/>
                          </InputGroup>
                          <div className="errores">{this.state.errores.errPassword}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CreateCiudad") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.ciudad} onChange={(event)=>this.setState({ciudad:event.target.value})} type="text" id="zona" name="Ciudad" placeholder={language("TuCiudad")} required={true}/>
                          </InputGroup>
                          <div className="errores">{this.state.errores.errCiudad}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("Barrio") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.Barrio} onChange={(event)=>this.setState({Barrio:event.target.value})} type="text" id="barrio" name="Barrio" placeholder={language("TuBarrio")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                         <FormGroup>
                            <div>
                              {language("CodigoPostal") +" *"}
                            </div>
                            <InputGroup>
                              <Input value={this.state.codigo_postal} onChange={(event)=>this.setState({codigo_postal:event.target.value})} type="number" id="codigopostal" name="CodigoPostal" placeholder="5151" required={true}/>
                            </InputGroup>
                            <div className="errores">{this.state.errores.errCodigoPostal}</div>
                          </FormGroup>
                          <FormGroup>
                          <div>
                            {language("Domicilio") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.domicilio} onChange={(event)=>this.setState({domicilio:event.target.value})} type="text" id="direccion" name="Direccion" placeholder={language("TuDireccion")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        </Col>


                        <Col>
                         <FormGroup>
                            <div>
                              {language("CreateApellido") +" *"}
                            </div>
                            <InputGroup>
                              <Input value={this.state.ap} onChange={(event)=>this.setState({ap:event.target.value})} type="text" id="apellido" name="apellido" placeholder={language("TuApellido")}  required={true}/>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup>
                            <div>
                              {language("NumeroTlf") +" *"}
                            </div>
                            <InputGroup>
                              <Input onChange={(event)=>this.setState({telefono:event.target.value})} type="number" id="telefono" name="telefono" value={this.state.telefono} placeholder={language("TuTelefono")}  required={true}/>
                            </InputGroup>
                            <div className="errores">{this.state.errores.errTelefono}</div>
                          </FormGroup>
                          <FormGroup>
                          <div>
                            {language("CreateEmailSecundario")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.email2} onChange={(event)=>this.setState({email2:event.target.value})} type="email" id="register_email_secundario" name="EmailSecundario" placeholder={language("TuCorreo")}  required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("Zona") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.zona} onChange={(event)=>this.setState({zona:event.target.value})} type="number" id="zona" name="Zona" placeholder={language("Zona")}  required={true}/>
                          </InputGroup>
                          <div className="errores">{this.state.errores.errZona}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("Provincia")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.provincia} onChange={(event)=>this.setState({provincia:event.target.value})} type="text" id="provincia" name="Provincia" placeholder={language("Provincia")}  required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CodigoBarrio")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.cod_barrio} onChange={(event)=>this.setState({cod_barrio:event.target.value})} type="number" id="codigobarrio" name="CodigoBarrio" placeholder="0000" required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CreateFechaNacimiento") +" *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.fecha_nac} onChange={(event)=>this.setState({fecha_nac:event.target.value})} type="date" id="direccion" name="Direccion" required={true}/>
                          </InputGroup>
                          <div className="errores">{this.state.errores.errFechaNac}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("MsjTypeFactura") +" *"}
                          </div>
                          <Input value={this.state.tipofactura} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({tipofactura:event.target.value})} required={true}>
                            <option value="-1">{language("OpcionSelecciona")}</option>
                            {
                              this.state.typeFactura.map((typefactura, i)=>{
                                return(
                                  <option value={typefactura.Desc}>{typefactura.Desc}</option>
                                );
                              })
                            }
                          </Input>
                          <div className="errores">{this.state.errores.errTipoFactura}</div>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("MsjIva") +" *"}
                          </div>
                          <InputGroup>

                            <Input value={this.state.condicioniva} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.setState({condicioniva:event.target.value})} required={true}>
                              <option value="-1">{language("OpcionSelecciona")}</option>
                              {
                                this.state.Ivas.map((iva, i)=>{
                                  return(
                                    <option value={iva.Desc}>{iva.Desc}</option>
                                  );
                                })
                              }
                            </Input>
                            <div className="errores">{this.state.errores.errIva}</div>
                          </InputGroup>
                        </FormGroup>
                        </Col>
                        </Row>
                        <Row>
                          { this.state.err ?
                           <div className="errores">{language("ErrorCampo")}</div>
                           : null
                          }
                        </Row>
                      </div>
                    :
                      <div className="flex-center" style={{height:'450px'}}>
                        <Loader
                           type="TailSpin"
                           color="#2ca8FF"
                           height="50"
                           width="50"
                        />
                      </div>
                    }
                  <ModalFooter >
                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateClient ? this.updateClient : this.createClient.bind(this)}>{ this.state.updateClient ? language('BotonEditar') : language("BotonRegistrar")}</Button>
                  </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Clientes;
