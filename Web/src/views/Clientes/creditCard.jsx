import React from 'react';
import {
    Button,  Row, Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card,
    CardBody,
    Pagination,
    PaginationItem,
    PaginationLink,
    Col
} from 'reactstrap';


import {
    PanelHeader
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import CreditCard from '../../api/services/creditCard';
import User from '../../api/services/user';

class users extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  users:[],
                  User:Cookies.load('userId'),
                  administrador:'0',
                  permiso:0,
                  err:false,
                  page:1,
                  loading:false,
                  disabled:false,
                  totalCredictCard:[],
                  total:[],
                  pageCreditCard:1,
                  creditCards:[]
                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.backPageCreditCard = this.backPageCreditCard.bind(this);
    this.addPageCreditCard = this.addPageCreditCard.bind(this);
    
  }
  
  componentWillReceiveProps(props){
    if(props.user)
    this.getDataCreditCard(this.state.page, props.user ? "personales_dni" : undefined, props.user ? props.user.id : undefined);

  }

  getDataCreditCard(page, search, value){
    CreditCard.All(this.state.User.access_token, page, search, value)
      .then(data=>{
          const totalCredictCard = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            totalCredictCard.push(i)  
          }
          
          this.setState({creditCards: data  ? data.data : [], totalCredictCard})
        });
  
  }

  addPageCreditCard() {
    if (this.state.pageCreditCard === this.state.totalCredictCard.length) {
      return;
    }
    this.getData(this.state.pageCreditCard+1);
    this.setState({pageCreditCard:this.state.pageCreditCard+1});
    
  }

  backPageCreditCard() {
    if (this.state.pageCreditCard === 1) {
      return;
    }
    this.getData(this.state.pageCreditCard-1);
    this.setState({pageCreditCard:this.state.pageCreditCard-1});
    
  }
 
  goToPageCre(index) {
    this.getData(index);
    this.setState({pageCreditCard:index});
    
  }
  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {
    this.setState({
      createUserModal: !this.state.createUserModal,
      nombre:'',
      usuario:'',
      email:'',
      id:null,
      updateUser:false,
      roles_idroles:1,
    });
  }

  createUser(){
    let bandera = true;
    let errores = {errores:{errNombre:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}};

    console.log(this.state)

    if (!this.state.nombretarjeta  || this.state.nombretarjeta  === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (!this.state.vencimientotarjeta || this.state.vencimientotarjeta === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (!this.state.numerotarjeta || this.state.numerotarjeta === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (!this.state.codigotarjeta || this.state.codigotarjeta === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (bandera) {
        this.state.personales_dni = this.props.user.id;
        this.setState({loadingCreditCard:true})
      CreditCard.Create(this.state.User.access_token,this.state)
        .then((data)=>{
          if (data.error) {
            swal('Oops...',data.error,'warning')
            this.setState({loadingCreditCard: false, disabled: false, created:true })
          } else {
            this.state.creditCards.push(data);
            this.setState({loadingCreditCard:false, disabled:false, created:false})
            swal(language("TarjetaCredito") + ` ${this.state.nombretarjeta} `+ language("MsjCreadoExito"),{icon:"success", button:false, timer:2000, loading: false, disabled: false})
          }
        })
    }
  }

  deleteListener(index) {
    swal({
      title: language("MsjElimnarTajerta") + ` ${this.state.creditCards[index].nombretarjeta} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      buttons:
      {
        exit:{
          text:language("BotonCancelar")
        },
        delete:{
          text:language("BotonEliminar"),
          className:'btn-danger'
        },


      }
    }).then(willDelete=>{
      if(willDelete!== 'delete') return;
      CreditCard.Delete(this.state.User.access_token, this.state.creditCards[index].id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjTarjetaEliminada") +` ${this.state.creditCards[index].nombretarjeta}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({creditCards:this.state.creditCards.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.creditCards[index].nombretarjeta}`, 'warning')
        })

    })


  }

  confirmPassword(event){
    if (event.target.value !== this.state.password) {
      this.setState( {passwordMatch: true} )
    } else {
      this.setState( {passwordMatch: false} )
    }
  }


  openUpdate(index){
    this.setState({...this.state.creditCards[index], created:true, updateUser:true, indexToEdit:index})
  }

  updateUser(){
     CreditCard.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        if (data.error) {
          swal('Oops...',data.error,'warning')
          this.setState({loadingCreditCard: false, disabled: false, created:true })
        } else {
          this.state.creditCards[this.state.indexToEdit] = data;
          this.setState({loadingCreditCard:false, disabled:false, created:false})
          swal(language("TarjetaCredito") +` ${this.state.nombretarjeta} ` + language("Editado"),{icon:"success", button:false, timer:2000, loading: false, disabled: false})
        }

      })
  }

  openCreditCardModal(index) {
    this.getDataCreditCard(this.state.pageCreditCard, 'personales_dni', this.props.users.dni)
    this.setState({creditCardModal:true})
  }
  closeCreditCardModal(){
    this.setState({created:false, nombretarjeta:null, vencimientotarjeta:null, numerotarjeta:null, codigotarjeta:null, banco:null})
  }
  toggle(){
      this.setState({created:false})
      this.props.toggle();
  }
  render(){
        return (
            <Modal className="boton-volver" size={this.state.created ?"md":"lg"} style={{}} isOpen={this.props.isOpen} toggle={this.toggle.bind(this)} >
                <ModalHeader  >
                    <table>
                        <tr>
                        <td>{ language('TarjetaCredito')}</td>
                        </tr>
                    </table>
                    </ModalHeader>
                   
                    { !this.state.loadingCreditCard ?
                    <Card>
                        <CardBody>
                            {!this.state.created ? 
                                <div>
                                { this.state.creditCards[0] != null ?
                                    <div>
                                    <Table hover responsive>
                                        <thead className="text-primary">
                                            <tr>
                                            <th className="justify-content-center text-center">{language("CreateName")}</th>
                                            <th className="justify-content-center text-center">{language("NumeroTarjeta")}</th>
                                            <th className="justify-content-center text-center">{language("VecimientoTarjeta")}</th>
                                            <th className="justify-content-center text-center">{language("Banco")}</th>
                                            <th className="justify-content-center text-center">{language("BotonEditar")}</th>
                                            <th className="justify-content-center text-center">{language("BotonEliminar")}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { this.state.creditCards.map((creditcard, i)=>{
                                            return(
                                                <tr key = {i}>
                                                <td className="justify-content-center text-center">
                                                    {creditcard.nombretarjeta}
                                                </td>
                                                <td className="justify-content-center text-center">
                                                    {creditcard.numerotarjeta}
                                                </td>
                                                <td className="justify-content-center text-center">
                                                    {creditcard.vencimientotarjeta}
                                                </td>
                                                <td className="justify-content-center text-center">
                                                    {creditcard.banco}
                                                </td>
                                                <td className="justify-content-center text-center">
                                                    <Button style={{width:'150px'}} color="primaryBlue" for="notification_file" onClick={()=>this.openUpdate(i)}>
                                                    <i className='now-ui-icons ui-2_settings-90'></i>
                                                    </Button>
                                                </td>
                                                <td className="justify-content-center text-center">
                                                    <Button style={{width:'150px'}} color="primaryBlue" for="notification_file" onClick={()=>this.deleteListener(i)}>
                                                    <i className='now-ui-icons ui-1_simple-remove'></i>
                                                    </Button>
                                                </td>
                                                </tr>
                                            );

                                            })}
                                        </tbody>
                                        </Table>
                                        
                                        <Pagination aria-label="Page navigation example">
                                        <PaginationItem>
                                            <PaginationLink style={{backgroundColor:this.state.pageCreditCard === 1 ? '#eee' : 'white'}} previous onClick={this.backPageCreditCard} />
                                        </PaginationItem>
                                        {this.state.totalCredictCard.map((data, i)=>{
                                            return(
                                            <PaginationItem>
                                                <PaginationLink style={{backgroundColor:this.state.pageCreditCard === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPageCreditCard(i+1)}>
                                                {i+1}
                                                </PaginationLink>
                                            </PaginationItem>
                                            );
                                        })}
                                        <PaginationItem>
                                            <PaginationLink style={{backgroundColor:this.state.pageCreditCard === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPageCreditCard} />
                                        </PaginationItem>
                                        </Pagination>
                                        </div>
                                        :
                                        <div className="content flex-center animated animatedFadeInUp fadeInUp">
                                            <h2 style={{'fontSize':'2em', 'marginTop':'60px'}}>{language("MsjCrearTarjetaCredito")}</h2>
                                        </div>
                                        }
                                </div>
                                :
                                <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>
                                    <FormGroup>
                                    <div>
                                        {language("CreateName")}
                                    </div>
                                    <InputGroup>
                                        <Input value={this.state.nombretarjeta} onChange={(event)=>this.setState({nombretarjeta:event.target.value, err:false})} type="text" id="register_name" name="Name" placeholder={language('CreateName')} required={true}/>
                                    </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                    <div>
                                        {language("NumeroTarjeta")}
                                    </div>
                                    <InputGroup>
                                        <Input value={this.state.numerotarjeta} onChange={(event)=>this.setState({numerotarjeta:event.target.value, err:false})} type="number" id="register_name" name="Name" placeholder="5899427821780011" required={true}/>
                                    </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                    <div>
                                        {language("VecimientoTarjeta")}
                                    </div>
                                    <InputGroup>
                                        <Input value={this.state.vencimientotarjeta} onChange={(event)=>this.setState({vencimientotarjeta:event.target.value, err:false})} type="date" id="register_email" name="Email" placeholder={language('VecimientoTarjeta')} required={true}/>
                                    </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                    <div>
                                        {language("Banco")}
                                    </div>
                                    <InputGroup>
                                        <Input value={this.state.banco} onChange={(event)=>this.setState({banco:event.target.value, err:false})} type="text" id="register_password" name="Password" placeholder={language("TuBanco")} required={true}/>
                                    </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                    <div>
                                        {language("CodigoTarjeta")}
                                    </div>
                                    <InputGroup>
                                        <Input value={this.state.codigotarjeta} onChange={(event)=>this.setState({codigotarjeta:event.target.value, err:false})} type="number" id="register_password" placeholder="234" required={true}/>
                                    </InputGroup>
                                    </FormGroup>
                                    
                                    { this.state.err ?
                                    <div className="errores">{language("ErrorCampo")}</div>
                                    : null
                                    }
                                </div>
                            }
                           
                        </CardBody>
                    </Card>

                    :

                    <div className="flex-center" style={{height:'350px'}}>
                        <Loader
                            type="TailSpin"
                            color="#2ca8FF"
                            height="50"
                            width="50"
                        />
                    </div>
                    }
                <ModalFooter>
                        {this.state.created && 
                            <div>
                            <Row>
                                <Col>
                                    <Button onClick={this.closeCreditCardModal.bind(this)}>
                                        {language("BotonRegresar")}
                                    </Button> 
                                </Col>
                                <Col>
                                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateUser ? this.updateUser.bind(this) : this.createUser.bind(this)}>
                                        { this.state.updateUser ? language('BotonEditar') : language("BotonRegistrar")}
                                    </Button>
                                </Col>
                            </Row>
                            </div>
                            
                        }
                </ModalFooter>
                {!this.state.created && <div style={{paddingBottom:'1%', paddingRight:'1%'}} className="fixed-right justify-content-right text-right">
                    <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={()=>this.setState({created:true})}>
                        <i className="now-ui-icons ui-1_simple-add"></i>
                    </Button>
                </div>}
                
            </Modal>
        );
    }
}

export default users;
