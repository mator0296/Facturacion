import Dashboard from '../views/Dashboard/Dashboard.jsx';
import Facturas from '../views/Facturas/Facturas.jsx';
import Clientes from '../views/Clientes/Clientes.jsx';
import Reclamos from '../views/Reclamos/Reclamos.jsx';
import Users from '../views/Users/Users.jsx';
import UserPage from '../views/UserPage/UserPage.jsx';
import Products from '../views/Products/Products.jsx';
import language from "../api/translator/translator"
import Recibos from '../views/Recibos/Recibos.jsx'
//import contacts from '../assets/icons/contacts.png'

var dashRoutes = [
    { path: "/facturas", name: language("Facturas"), icon: "files_paper", component: Facturas },
    { path: "/reclamos", name: language("Reclamos"), icon: "design_bullet-list-67", component: Reclamos },
    { path: "/recibos", name: language("Recibos"), icon: "design_bullet-list-67", component: Recibos },
    { path: "/clientes", name: language("Clientes"), icon: "business_badge", component: Clientes },
    { path: "/usuarios", name: language("Usuarios"), icon: "users_circle-08", component: Users},
    { path: "/productos", name: language("Productos"), icon: "users_circle-08", component: Products},
    { path: "/user-profile", name: language("Perfil"), component: UserPage, invisible: true },
    { redirect: true, path: "/", pathTo: "/facturas", name: "Facturas" }
];
export default dashRoutes;
