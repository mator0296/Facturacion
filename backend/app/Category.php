<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function events() {
        return $this->belongsToMany('App\Event', 'categories_events', 'categories_id', 'events_id');
    }

    public function profiles() {
        return $this->belongsToMany('App\Profile', 'categories_profiles', 'categories_id', 'profiles_id');
    }
}
