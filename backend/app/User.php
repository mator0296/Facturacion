<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    protected $table = 'usuarios';

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'usuario', 'nombre', 'created_at', 'updated_at', 'id' , 'deleted_at', 'roles_idroles'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'clave'
    ];
    /*public function Rol() {
        return $this->belongsTo('App\Role', 'users_id');
    }

    public function Profile() {
        return $this->hasOne('App\Profile', 'user_id');
    }

    public function proposals() {
        return $this->hasMany('App\Proposal', 'users_id');
    }*/



  public function clients() {
    return $this->belongsToMany('App\Client', 'personales_has_usuarios', 'usuarios_id', 'personales_id');
  }

    public $timestamps = false;
}
