<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $table = 'facturas';

    protected $fillable = [
    	"id",
		"id_factura",
		"codcliente",
		"codplan",
		"dni",
		"nombre",
		"cod_producto",
		"num_factura",
		"descripcion",
		"vencimiento",
		"mora",
		"vencimiento2",
		"debe",
		"saldo",
		"observada",
		"leyendaobservada",
		"clase",
		"haber",
		"hora",
		"fecha_sistema",
		"cod_mes",
		"pagado",
		"periodo",
		"fecha_pago",
		"importe_pagado",
		"recibo",
		"codoperario",
		"forma",
		"estado",
		"entrega",
		"bon_intereses",
		"observaciones",
		"pagosraul",
		"fechapagoraul",
		"reciboraul",
		"sergiopagado",
		"observacionessergio",
		"codenviado",
		"ZONA",
		"tipo",
		"cae",
		"caevencimiento",
		"caeentregado",
    ];

    public $timestamps = false;

}
