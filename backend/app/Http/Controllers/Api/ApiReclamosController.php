<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reclamos;
use App\Client;
use App\Factura;
use Hash;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class ApiReclamosController extends Controller
{
    public function Create(Request $request)
    {
    	$reclamo = new Reclamos;

        $reclamo->id = Reclamos::all()->count()+1;
    	if ($request->descripcion)
        	$reclamo->descripcion = $request->descripcion;

        if ($request->personales_dni)
        	$reclamo->personales_dni = $request->personales_dni;

        if ($request->facturas_id)
            $reclamo->facturas_id_factura = $request->facturas_id;


        $cliente = Client::where('dni',$reclamo['personales_dni'])->first();
        $factura = Factura::where('id_factura',$reclamo['facturas_id_factura'])->first();
        DB::insert('insert into reclamos (
            descripcion,
            personales_dni,
            facturas_id_factura
        ) values (
            ?, ?, ?
        )', [
            $reclamo->descripcion,
            $reclamo->personales_dni,
            $reclamo->facturas_id_factura
        ]);
		return array("reclamo"=>Reclamos::all()->last(), "nombre"=>$cliente->nombre, "num_factura"=>$factura->num_factura);
    }

    public function createClientTicket(Request $request)
    {
      $reclamo = new Reclamos;

      $reclamo->id = Reclamos::all()->count()+1;

      if ($request->descripcion)
          $reclamo->descripcion = $request->descripcion;

      if ($request->personales_dni)
        $reclamo->personales_dni = $request->personales_dni;

      if ($request->facturas_id)
          $reclamo->facturas_id_factura = $request->facturas_id;

      $cliente = Client::where('dni',$reclamo['personales_dni'])->first();

      DB::insert('insert into reclamos (
        descripcion,
        personales_dni,
        facturas_id_factura
      ) values (
          ?, ?, ?
        )', [
            $reclamo->descripcion,
            $reclamo->personales_dni,
            $reclamo->facturas_id_factura
          ]);

    return array("reclamo"=>Reclamos::all()->last(), "nombre"=>$cliente->nombre);
    }

    public function Edit(Request $request) {



    	$reclamo = [];

    	if ($request->descripcion)
            $reclamo['descripcion'] = $request->descripcion;

        if ($request->personales_id)
            $reclamo['personales_dni'] = $request->personales_id;

        if ($request->facturas_id)
            $reclamo['facturas_id_factura'] = $request->facturas_id;

        $cliente = Client::where('dni',$reclamo['personales_dni'])->first();
        $factura = Factura::where('id_facturas',$reclamo['facturas_id_factura'])-first();
        Reclamos::where('id', $request->id)->update($reclamo);
        $reclamo = Reclamos::where('id', $request->id)->first();

		return ["reclamo"=>$reclamo, "nombre"=> $personales->nombre, "num_factura"=>$factura->num_factura];
	}

  public function updateClientTicket(Request $request) {
    $reclamo = [];

    if ($request->descripcion)
      $reclamo['descripcion'] = $request->descripcion;

    if ($request->personales_dni)
      $reclamo['personales_dni'] = $request->personales_dni;

    if ($request->facturas_id)
      $reclamo['facturas_id_factura'] = $request->facturas_id;

    $cliente = Client::where('dni',$reclamo['personales_dni'])->first();
    Reclamos::where('id', $request->id)->update($reclamo);
    $reclamo = Reclamos::where('id', $request->id)->first();

    return array("reclamo"=>$reclamo, "nombre"=> $cliente->nombre);
}

	public function Delete(Request $request)
    {


        $reclamo = Reclamos::where('id',$request->id)->first();
        $reclamo->delete();

        return response()->json(['reclamo' => $reclamo], 200);
    }


    public function All(Request $request)
    {
		if($request->page === 'undefined'){
            $reclamos = Reclamos::all();
            $allreclamos = [];
            foreach ($reclamos as $key => $value) {
                $cliente = Client::where('dni',$value->personales_dni)->first();
                $factura = Factura::where('id_factura',$value->facturas_id_factura)->first();
                array_push($allreclamos, [
                    "reclamo" => $value,
                    "nombre" => $cliente->nombre,
                    "num_factura" => $factura->num_factura,
                    "descripcion" => $factura->descripcion,
                ]);
            }
            return $allreclamos;
        }
        $reclamos = Reclamos::paginate(5);
        $allreclamos = [];
        foreach ($reclamos as $value) {
            $cliente = Client::where('dni',$value->personales_dni)->first();
            $factura = Factura::where('id_factura',$value->facturas_id_factura)->first();
            array_push($allreclamos, [
                "reclamo" => $value,
                "nombre" => $cliente->nombre,
                "num_factura" => $factura->num_factura,
                "descripcion" => $factura->descripcion,
            ]);
        }
        return ["data"=>$allreclamos, "total"=> Reclamos::count()];
    }

    public function Reclamos($id)
    {
    	return Reclamos::where('id',$id)->first();
    }

    public function clientTickets($dni) {
        $reclamos = Reclamos::where('personales_dni',$dni)->get();

        for ($i=0; $i < count($reclamos) ; $i++) { 
            $factura = Factura::where('id',$reclamos[$i]->facturas_id_factura)->first();
            $reclamos[$i]->num_factura = $factura->num_factura;
            $reclamos[$i]->descripcion_factura = $factura->descripcion;
        }

      return $reclamos;
    }
}
