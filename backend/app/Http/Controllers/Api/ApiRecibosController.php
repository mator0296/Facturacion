<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mpdf\Mpdf;
use App\Recibo;
use App\Factura;
use App\Client;
use App\Product;
use App\Facturas_has_articulo_det;
use Hash;
use Illuminate\Support\Facades\DB;
use JWTAuth;

use NumerosEnLetras;

class ApiRecibosController extends Controller
{
    public function Create(Request $request)
    {

    	$recibo = new Recibo;


        $recibo->codfactura = $request->facturas_id;

        if ($request->fecha)
        	$recibo->fecha = $request->fecha;

        if ($request->codmes)
        	$recibo->codmes = $request->codmes;

        if ($request->descripcion)
        	$recibo->descripcion = $request->descripcion;

        if ($request->dni)
        	$recibo->dni = $request->dni;

        if ($request->importe)
        	$recibo->importe = $request->importe;

        if ($request->importe_original)
        	$recibo->importe_original = $request->importe_original;

        if ($request->intereses)
        	$recibo->intereses = $request->intereses;

        if ($request->operador)
        	$recibo->operador = $request->operador;

        if ($request->codmes_fecha)
        	$recibo->codmes_fecha = $request->codmes_fecha;

        if ($request->zona)
        	$recibo->zona = $request->zona;

        if ($request->codoperacion)
        	$recibo->codoperacion = $request->codoperacion;

        if ($request->ano)
        	$recibo->ano = $request->ano;

        if ($request->forma)
        	$recibo->forma = $request->forma;

        if ($request->comprobante_tarjeta)
        	$recibo->comprobante_tarjeta = $request->comprobante_tarjeta;

        if ($request->TIPO)
        	$recibo->TIPO = $request->TIPO;

        DB::insert('insert into recibos
        (
            codfactura,
            fecha,
            codmes,
            descripcion,
            importe,
            importe_original,
            intereses,
            operador,
            codmes_fecha,
            zona,
            codoperacion,
            dni,
            ano,
            forma,
            comprobante_tarjeta,
            TIPO
        ) values (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )',
        [
            $recibo->codfactura,
            $recibo->fecha,
            $recibo->codmes,
            $recibo->descripcion,
            $recibo->importe,
            $recibo->importe_original,
            $recibo->intereses,
            $recibo->operador,
            $recibo->codmes_fecha,
            $recibo->zona,
            $recibo->codoperacion,
            $recibo->dni,
            $recibo->ano,
            $recibo->forma,
            $recibo->comprobante_tarjeta,
            $recibo->TIPO
        ]);


        //$recibo->lote = Recibo::all()->count()+1;





		return Recibo::all()->last();
    }

    public function Edit(Request $request) {


    	$recibo = [];

    	if ($request->facturas_id)
        	$recibo['codfactura'] = $request->facturas_id;

        if ($request->fecha)
        	$recibo['fecha'] = $request->fecha;

        if ($request->codmes)
        	$recibo['codmes'] = $request->codmes;

        if ($request->descripcion)
        	$recibo['descripcion'] = $request->descripcion;

        if ($request->importe)
        	$recibo['importe'] = $request->importe;

        if ($request->importe_original)
        	$recibo['importe_original'] = $request->importe_original;

        if ($request->intereses)
        	$recibo['intereses'] = $request->intereses;

        if ($request->operador)
        	$recibo['operador'] = $request->operador;

        if ($request->codmes_fecha)
        	$recibo['codmes_fecha'] = $request->codmes_fecha;

        if ($request->zona)
        	$recibo['zona'] = $request->zona;

        if ($request->codoperacion)
        	$recibo['codoperacion'] = $request->codoperacion;

        if ($request->ano)
        	$recibo['ano'] = $request->ano;

        if ($request->forma)
        	$recibo['forma'] = $request->forma;

        if ($request->comprobante_tarjeta)
        	$recibo['comprobante_tarjeta'] = $request->comprobante_tarjeta;

        if ($request->TIPO)
        	$recibo['TIPO'] = $request->TIPO;

        if ($request->dni)
        	$recibo->dni = $request->dni;


        Recibo::where('id',$request->id)->update($recibo);


        $factura = Factura::where('id',$recibo->codfactura)->first();

        $factura->haber = $factura->haber - $recibo->importe;

        $factura->save();

		return Recibo::where('id',$request->id)->first();
    }

    public function Delete(Request $request)
    {


        $recibo = Recibo::where('id',$request->id);
        $recibo->delete();
        return response()->json(['recibo' => $recibo], 200);
    }

    public function All(Request $request)
    {
        if($request->page === 'undefined'){
            if($request->search === 'undefined'){
               $i = 0;
              $data = [];
              $users = Recibo::all();
              foreach ( $users as $key => $value) {
                foreach ($value->getAttributes() as $key2=> $data2) {
                  $value[$key2]= utf8_encode($value[$key2]);

                  }
                array_push($data, $value);
              }
              return $data;
            }

            $i = 0;
              $data = [];
              $users = Recibo::where($request->search,$request->value)->get();
              foreach ( $users as $key => $value) {
                foreach ($value->getAttributes() as $key2=> $data2) {
                  $value[$key2]= utf8_encode($value[$key2]);

                  }
                array_push($data, $value);
              }
              return $data;

        }

		if($request->search === 'undefined'){
           $i = 0;
          $data = [];
          $users = Recibo::all();
          $total = Recibo::all()->count();
          foreach ( $users as $key => $value) {
            if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
                foreach ($value->getAttributes() as $key2=> $data2) {
                  $value[$key2]= utf8_encode($value[$key2]);

                  }
                array_push($data, $value);
            }
          }
          return ['data'=>$data , 'total'=>$total];
        }

        $i = 0;
        $data = [];
        $total = Recibo::where($request->search, $request->value)->count();
        $users = Recibo::where($request->search, $request->value)->get();
        foreach ( $users as $key => $value) {
            if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
                foreach ($value->getAttributes() as $key2=> $data2) {

                  $value[$key2]= utf8_encode($value[$key2]);

                  }
                array_push($data, $value);
            }
        }
        return ['data'=>$data , 'total'=>$total];;

    }

    public function Recibo($id)
    {
    	return Recibo::find($id);
    }

    public function getGenerar(Request $request)
    {
        $accion = $request->get('accion');
        $tipo = $request->get('tipo');
        $id_recibo = $request->get('id');
        return $this->pdf($accion,$tipo, $id_recibo);
    }

    public function pdf($accion,$tipo, $id)
    {
        $recibo = Recibo::where('id',$id)->first();
        $factura = Factura::where('id',$recibo->codfactura)->first();
        $client = Client::where('dni',$factura->dni)->first();

        $numero = $recibo->id;
        $nombres = $client->nombre . " " . $client->ap;
        $dates =  explode('-', $recibo->fecha);
        $importe = $recibo->importe;
        $descripcion = $recibo->descripcion;
        $forma = $recibo->forma;

        $dni = $client->dni;

        $total = 0;
        $facturasArt = Facturas_has_articulo_det::where('facturas_id', $factura->id)->get();
         $articulos = [];
        foreach ($facturasArt as $key => $value) {
            $product = Product::where('codigo_art',$value->articulo_det_id)->first();
            array_push($articulos, [
                "cantidad" => $value->cantidad,
                "descripcion" => $product->detalle_art,
                "precio" => $product->unitario,
                "importe" => $value->cantidad*$product->unitario,
            ]);
        }

        foreach ($articulos as $key => $value) {
            $total += $value["importe"];
            $articulos[$key]["precio"] = number_format($value["precio"],2,'.',' ');;
            $articulos[$key]["importe"] = number_format($value["importe"],2,'.',' ');;

        }
        $total = number_format($total,2,'.',' ');
        $fecha_actual = getdate();


        $data['numero'] = $numero;
        $data['nombre'] = $nombres;
        $data['dia'] = substr($dates[2], 0, strpos($dates[2], " "));
        $data['mes'] = $dates[1];
        $data['ayo'] = $dates[0];
        //$data['dia_actual'] = $fecha_actual[wday];
        //$data['mes_actual'] = $fecha_actual[month];
        //$data['ayo_actual'] = $fecha_actual[year];
        $data['descripcion'] = $descripcion;
        $data['dni'] = $dni;
        $data['importe'] = $importe;
        $data['importeString'] = NumerosEnLetras::convertir($importe,'Pesos',false,'Centavos');
        $data['articulos'] = $articulos;
        $data['total'] = $total;
        $data['forma'] = $forma;

        if($accion=='get'){
            return view('recibo.index',$data);
        }else{
            $html = view('recibo.index',$data)->render();
        }

        $namefile = 'recibo_de_pago_'.time().'.pdf';

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                public_path() . '/fonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'sueca-medium.ttf',
                    'B' => 'sueca-bold.ttf',
                ],
            ],
            'default_font' => 'arial',
            // "format" => "A4",
            "format" => [264.8,188.9],
        ]);

        // $mpdf->SetTopMargin(5);
        $mpdf->SetDisplayMode('fullpage');


        $mpdf->WriteHTML($html);
        // dd($mpdf);
        if($accion=='get'){
            $mpdf->Output($namefile,"I");
        }else if($accion=='descargar'){
            $mpdf->Output($namefile,"D");
        }

    }

    public function clientReceipts($dni) {
      return Recibo::where("dni", $dni)->get();
    }

}
