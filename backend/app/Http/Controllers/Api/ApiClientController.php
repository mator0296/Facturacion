<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Client;
use App\User;
use Hash;
use JWTAuth;

class ApiClientController extends Controller
{
    public function Create(Request $request)
    {

      $clients = Client::all();
      $users = User::all();

      foreach ($clients as $key => $value) {
        if ($value['dni'] == request('dni')) {
          return response()-> json(['error' => 'Este DNI ya existe.']);
        }
      }


  	  $client = new Client;


      if($request->dni)
        $client->dni = $request->dni;

      if($request->nombre)
        $client->nombre = $request->nombre;

      if($request->domicilio)
        $client->domicilio = $request->domicilio;

      if($request->telefono)
        $client->telefono = $request->telefono;

      if($request->email)
        $client->email = $request->email;

      if($request->email2)
        $client->email2 = $request->email2;

      if($request->Barrio)
        $client->Barrio = $request->Barrio;

      if($request->codigo_postal)
        $client->codigo_postal = $request->codigo_postal;

      if($request->ciudad)
        $client->ciudad = $request->ciudad;

      if($request->zona)
        $client->zona = $request->zona;

      if($request->cod_barrio)
        $client->cod_barrio = $request->cod_barrio;

      if($request->provincia)
        $client->provincia = $request->provincia;

      if($request->fecha_nac)
        $client->fecha_nac = $request->fecha_nac;

      if($request->ap)
        $client->ap = $request->ap;

      if($request->servidor)
        $client->servidor = $request->servidor;

      if($request->nodo)
        $client->nodo = $request->nodo;

      if($request->ubicacion_nodo)
        $client->ubicacion_nodo = $request->ubicacion_nodo;

      if($request->numero_servidor)
        $client->numero_servidor = $request->numero_servidor;

      if($request->servidor_secundario)
        $client->servidor_secundario = $request->servidor_secundario;

      if($request->tipo_equipo)
        $client->tipo_equipo = $request->tipo_equipo;

      if($request->equipo)
        $client->equipo = $request->equipo;

      if($request->ip)
        $client->ip = $request->ip;

      if($request->ipequipo)
        $client->ipequipo = $request->ipequipo;

      if($request->mac)
        $client->mac = $request->mac;

      if($request->fecha_alta)
        $client->fecha_alta = $request->fecha_alta;

      if($request->hora)
        $client->hora = $request->hora;

      if($request->velocidad)
        $client->velocidad = $request->velocidad;

      if($request->baja)
        $client->baja = $request->baja;

      if($request->cuota)
        $client->cuota = $request->cuota;

      if($request->bajadetalle)
        $client->bajadetalle = $request->bajadetalle;

      if($request->baja_provisoria)
        $client->baja_provisoria = $request->baja_provisoria;

      if($request->fecha_provisoria)
        $client->fecha_provisoria = $request->fecha_provisoria;

      if($request->fecha_baja)
        $client->fecha_baja = $request->fecha_baja;

      if($request->tolerancia)
        $client->tolerancia = $request->tolerancia;

      if($request->fecha_instalacion)
        $client->fecha_instalacion = $request->fecha_instalacion;

      if($request->chacra)
        $client->chacra = $request->chacra;

      if($request->tolhuin)
        $client->tolhuin = $request->tolhuin;

      if($request->instalar)
        $client->instalar = $request->instalar;

      if($request->fecha_instalado)
        $client->fecha_instalado = $request->fecha_instalado;

      if($request->obs_instala)
        $client->obs_instala = $request->obs_instala;

      if($request->centro_ventas)
        $client->centro_ventas = $request->centro_ventas;

      if($request->vencimiento_factura)
        $client->vencimiento_factura = $request->vencimiento_factura;

      if($request->errorcorreo)
        $client->errorcorreo = $request->errorcorreo;

      if($request->trabajo)
        $client->trabajo = $request->trabajo;

      if($request->contrato)
        $client->contrato = $request->contrato;

      if($request->orden)
        $client->orden = $request->orden;

      if($request->condeuda)
        $client->condeuda = $request->condeuda;

      if($request->baja_equipo)
        $client->baja_equipo = $request->baja_equipo;

      if($request->ok)
        $client->ok = $request->ok;

      if($request->barrapersonal)
        $client->barrapersonal = $request->barrapersonal;

      if($request->adios)
        $client->adios = $request->adios;

      if($request->comopaga)
        $client->comopaga = $request->comopaga;

      if($request->red)
        $client->red = $request->red;

      if($request->idap)
        $client->idap = $request->idap;

      if($request->idservidor)
        $client->idservidor = $request->idservidor;

      if($request->idantena)
        $client->idantena = $request->idantena;

      if($request->tipofactura)
        $client->tipofactura = $request->tipofactura;

      if($request->tipodoc)
        $client->tipodoc = $request->tipodoc;

      if($request->codtipodoc)
        $client->codtipodoc = $request->codtipodoc;

      if($request->condicioniva)
        $client->condicioniva = $request->condicioniva;

      if($request->empresa)
        $client->empresa = $request->empresa;

      try {
          DB::insert('insert into personales (
              dni,
              nombre,
              domicilio,
              telefono,
              email,
              email2,
              Barrio,
              codigo_postal,
              ciudad,
              zona,
              cod_barrio,
              provincia,
              servidor,
              nodo,
              ubicacion_nodo,
              numero_servidor,
              servidor_secundario,
              tipo_equipo,
              equipo,
              ip,
              ipequipo,
              mac,
              fecha_alta,
              hora,
              velocidad,
              baja,
              cuota,
              bajadetalle,
              baja_provisoria,
              fecha_provisoria,
              fecha_baja,
              fecha_nac,
              tolerancia,
              fecha_instalacion,
              chacra,
              tolhuin,
              instalar,
              fecha_instalado,
              obs_instala,
              centro_ventas,
              vencimiento_factura,
              errorcorreo,
              trabajo,
              contrato,
              orden,
              condeuda,
              baja_equipo,
              ok,
              barrapersonal,
              adios,
              comopaga,
              red,
              idap,
              ap,
              idservidor,
              idantena,
              tipofactura,
              tipodoc,
              codtipodoc,
              condicioniva,
              empresa,
              debito,
              banco,
              tarjeta,
              nombretarjeta,
              numerotarjeta,
              vencimientotarjeta,
              codigotarjeta,
              codmes,
              ano
            ) values (
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?,
              ?
            )', [
              $client->dni,
              $client->nombre,
              $client->domicilio,
              $client->telefono,
              $client->email,
              $client->email2,
              $client->Barrio,
              $client->codigo_postal,
              $client->ciudad,
              $client->zona,
              $client->cod_barrio,
              $client->provincia,
              $client->servidor,
              $client->nodo,
              $client->ubicacion_nodo,
              $client->numero_servidor,
              $client->servidor_secundario,
              $client->tipo_equipo,
              $client->equipo,
              $client->ip,
              $client->ipequipo,
              $client->mac,
              $client->fecha_alta,
              $client->hora,
              $client->velocidad,
              $client->baja,
              $client->cuota,
              $client->bajadetalle,
              $client->baja_provisoria,
              $client->fecha_provisoria,
              $client->fecha_baja,
              $client->fecha_nac,
              $client->tolerancia,
              $client->fecha_instalacion,
              $client->chacra,
              $client->tolhuin,
              $client->instalar,
              $client->fecha_instalado,
              $client->obs_instala,
              $client->centro_ventas,
              $client->vencimiento_factura,
              $client->errorcorreo,
              $client->trabajo,
              $client->contrato,
              $client->orden,
              $client->condeuda,
              $client->baja_equipo,
              $client->ok,
              $client->barrapersonal,
              $client->adios,
              $client->comopaga,
              $client->red,
              $client->idap,
              $client->ap,
              $client->idservidor,
              $client->idantena,
              $client->tipofactura,
              $client->tipodoc,
              $client->codtipodoc,
              $client->condicioniva,
              $client->empresa,
              $client->debito,
              $client->banco,
              $client->tarjeta,
              $client->nombretarjeta,
              $client->numerotarjeta,
              $client->vencimientotarjeta,
              $client->codigotarjeta,
              $client->codmes,
              $client->ano
            ]);



      } catch (Exception $e) {
         return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
      }

		return Client::all()->last();
    }

    public function Edit(Request $request)
    {
      $clients = Client::all();


      foreach ($clients as $key => $value) {
        if ($value['dni'] == request('dni') && $value['id'] != request('id')) {
          return response()-> json(['error' => 'Este DNI ya existe.']);
        }
      }

    	$client = [];


  		if($request->dni)
  			$client['dni'] = $request->dni;

  		if($request->nombre)
  			$client['nombre'] = $request->nombre;

  		if($request->domicilio)
  			$client['domicilio'] = $request->domicilio;

  		if($request->telefono)
  			$client['telefono'] = $request->telefono;

  		if($request->email)
  			$client['email'] = $request->email;

  		if($request->email2)
  			$client['email2'] = $request->email2;

  		if($request->Barrio)
  			$client['Barrio'] = $request->Barrio;

  		if($request->codigo_postal)
  			$client['codigo_postal'] = $request->codigo_postal;

  		if($request->ciudad)
  			$client['ciudad'] = $request->ciudad;

  		if($request->zona)
  			$client['zona'] = $request->zona;

  		if($request->cod_barrio)
  			$client['cod_barrio'] = $request->cod_barrio;

  		if($request->provincia)
  			$client['provincia'] = $request->provincia;

      if($request->fecha_nac)
    		$client['fecha_nac'] = $request->fecha_nac;

      if($request->ap)
    		$client['ap'] = $request->ap;

  		if($request->servidor)
  			$client['servidor'] = $request->servidor;

  		if($request->nodo)
  			$client['nodo'] = $request->nodo;

  		if($request->ubicacion_nodo)
  			$client['ubicacion_nodo'] = $request->ubicacion_nodo;

  		if($request->numero_servidor)
  			$client['numero_servidor'] = $request->numero_servidor;

  		if($request->servidor_secundario)
  			$client['servidor_secundario'] = $request->servidor_secundario;

  		if($request->tipo_equipo)
  			$client['tipo_equipo'] = $request->tipo_equipo;

  		if($request->equipo)
  			$client['equipo'] = $request->equipo;

  		if($request->ip)
  			$client['ip'] = $request->ip;

  		if($request->ipequipo)
  			$client['ipequipo'] = $request->ipequipo;

  		if($request->mac)
  			$client['mac'] = $request->mac;

  		if($request->fecha_alta)
  			$client['fecha_alta'] = $request->fecha_alta;

  		if($request->hora)
  			$client['hora'] = $request->hora;

  		if($request->velocidad)
  			$client['velocidad'] = $request->velocidad;

  		if($request->baja)
  			$client['baja'] = $request->baja;

  		if($request->cuota)
  			$client['cuota'] = $request->cuota;

  		if($request->bajadetalle)
  			$client['bajadetalle'] = $request->bajadetalle;

  		if($request->baja_provisoria)
  			$client['baja_provisoria'] = $request->baja_provisoria;

  		if($request->fecha_provisoria)
  			$client['fecha_provisoria'] = $request->fecha_provisoria;

  		if($request->fecha_baja)
  			$client['fecha_baja'] = $request->fecha_baja;

  		if($request->tolerancia)
  			$client['tolerancia'] = $request->tolerancia;

  		if($request->fecha_instalacion)
  			$client['fecha_instalacion'] = $request->fecha_instalacion;

  		if($request->chacra)
  			$client['chacra'] = $request->chacra;

  		if($request->tolhuin)
  			$client['tolhuin'] = $request->tolhuin;

  		if($request->instalar)
  			$client['instalar'] = $request->instalar;

  		if($request->fecha_instalado)
  			$client['fecha_instalado'] = $request->fecha_instalado;

  		if($request->obs_instala)
  			$client['obs_instala'] = $request->obs_instala;

  		if($request->centro_ventas)
  			$client['centro_ventas'] = $request->centro_ventas;

  		if($request->vencimiento_factura)
  			$client['vencimiento_factura'] = $request->vencimiento_factura;

  		if($request->errorcorreo)
  			$client['errorcorreo'] = $request->errorcorreo;

  		if($request->trabajo)
  			$client['trabajo'] = $request->trabajo;

  		if($request->contrato)
  			$client['contrato'] = $request->contrato;

  		if($request->orden)
  			$client['orden'] = $request->orden;

  		if($request->condeuda)
  			$client['condeuda'] = $request->condeuda;

  		if($request->baja_equipo)
  			$client['baja_equipo'] = $request->baja_equipo;

  		if($request->ok)
  			$client['ok'] = $request->ok;

  		if($request->barrapersonal)
  			$client['barrapersonal'] = $request->barrapersonal;

  		if($request->adios)
  			$client['adios'] = $request->adios;

  		if($request->comopaga)
  			$client['comopaga'] = $request->comopaga;

  		if($request->red)
  			$client['red'] = $request->red;

  		if($request->idap)
  			$client['idap'] = $request->idap;

  		if($request->idservidor)
  			$client['idservidor'] = $request->idservidor;

  		if($request->idantena)
  			$client['idantena'] = $request->idantena;

  		if($request->tipofactura)
  			$client['tipofactura'] = $request->tipofactura;

  		if($request->tipodoc)
  			$client['tipodoc'] = $request->tipodoc;

  		if($request->codtipodoc)
  			$client['codtipodoc'] = $request->codtipodoc;

  		if($request->condicioniva)
  			$client['condicioniva'] = $request->condicioniva;

  		if($request->empresa)
			 $client['empresa'] = $request->empresa;

		  Client::where('id', $request->id)->update($client);

		return Client::where('id', $request->id)->first();
    }

    

    public function Delete(Request $request)
    {
      try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }

      $client = Client::where('id', $request->id)->first();

      $client->delete();
      return response()->json(['client' => $client], 200);
    }
  public function utf8_encode_deep(&$input) {
    if (is_string($input)) {
      $input = utf8_encode($input);
    } else if (is_array($input)) {
      foreach ($input as &$value) {
        $utf8_encode_deep($value);
      }
      
      unset($value);
    } else if (is_object($input)) {
      $vars = array_keys(get_object_vars($input));
      
      foreach ($vars as $var) {
        $utf8_encode_deep($input->$var);
      }
    }
  }


    public function All(Request $request)
    {
        if($request->page === 'undefined'){
          $i = 0;
          $data = [];
          $users = Client::orderBy('nombre','asc')->get();
          foreach ( $users as $key => $value) {
            foreach ($value->getAttributes() as $key2=> $data2) {
              $value[$key2]= utf8_encode($value[$key2]);
              error_log($value[$key2]);
              }                           
            array_push($data, $value);
          }
          return $data;
        }

      $total = Client::all()->count();
      $i = 0;
      $data = [];
      $users = Client::orderBy('nombre','asc')->get();
      foreach ( $users as $key => $value) {
          if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
            array_push($data, $value);
          }
          $i++;
      }

      return ['data'=>$data  , 'total'=>$total];
    }



    public function client($id)
    {
    	return Client::where('id',$id)->first();
    }
}
