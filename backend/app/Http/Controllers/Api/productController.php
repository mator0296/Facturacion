<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Product;
use JWTAuth;

class productController extends Controller
{

    public function Create(Request $request)
    {

    	
        $products = Product::all();

        foreach ($products as $key => $value) {
          if ($value['codigo_art'] == request('codigo_art')) {
            return response()-> json(['error' => 'Ya existe un articulo con este código.']);
          }
        }

    	/*
        $product = [];
        if ($request->codigo_art != null)
            $product['codigo_art'] = $request->codigo_art;
        
        if ($request->detalle_art != null)
            $product['detalle_art'] = $request->detalle_art;

        if ($request->unitario != null)
            $product['unitario'] = $request->unitario;

        if ($request->porcentaje != null)
            $product['porcentaje'] = $request->porcentaje;
        */

        $product = new Product;
        if ($request->codigo_art != null)
            $product->codigo_art = $request->codigo_art;
        
        if ($request->detalle_art != null)
            $product->detalle_art = $request->detalle_art;

        if ($request->unitario != null)
            $product->unitario = $request->unitario;

        if ($request->porcentaje != null)
            $product->porcentaje = $request->porcentaje;
		
        DB::insert('insert into articulo_det (
            codigo_art,
            detalle_art,
            unitario,
            porcentaje
        ) values (
            ?, ?, ?, ?
        )', [
            $product->codigo_art,
            $product->detalle_art,
            $product->unitario,
            $product->porcentaje
        ]);

		return Product::all()->last();
    }

    public function Edit(Request $request)
    {
    	
        $products = Product::all();

        foreach ($products as $key => $value) {
          if ($value['codigo_art'] == request('codigo_art') && $value['id_ar'] == request('id_ar')) {
            continue;
          } elseif ($value['codigo_art'] == request('codigo_art')) {
            return response()-> json(['error' => 'Ya existe un articulo con este codigo.']);
          }
        }

    	
        $product = [];
		
		if ($request->detalle_art != null)
			$product['detalle_art'] = $request->detalle_art;
        if ($request->codigo_art != null)
            $product['codigo_art'] = $request->codigo_art;

		if ($request->unitario != null)
			$product['unitario'] = $request->unitario;

		if ($request->porcentaje != null)
			$product['porcentaje'] = $request->porcentaje;



		Product::where('id_ar', $request->id_ar)->update($product);

		return $product = Product::where('id_ar', $request->id_ar)->first();;
    }

    public function Delete(Request $request)
    {
        
        $prod = Product::where('id_ar', $request->id_ar)->first();
        $prod->delete();
        return response()->json(['prod' => $prod], 200);
    }

    public function All(Request $request)
    {
        if($request->page === 'undefined'){
          $i = 0;
          $data = [];
          $users = Product::all();
          foreach ( $users as $key => $value) {
            foreach ($value->getAttributes() as $key2=> $data2) {
              $value[$key2]= utf8_encode($value[$key2]);
              error_log($value[$key2]);
              }                           
            array_push($data, $value);
          }
          return $data;
        }

    	$total = Product::all()->count();
        $i = 0;
        $data = [];
        $users =Product::all();
        foreach ( $users as $key => $value) {
          if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
            array_push($data, $value);
          }
          $i++;
        }
        
        return ['data'=>$data , 'total'=>$total];
    }

    public function Product($id)
    {
    	return Product::where('id_ar', $id);
    }
}
