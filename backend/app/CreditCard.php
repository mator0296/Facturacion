<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
     /**
    * The database table used by the model.
    *
    * @var string
    */


    protected $table = 'credit_card';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'debito',
        'nombretarjeta',
        'numerotarjeta',
        'vencimientotarjeta',
        'codigotarjeta',
        'codmes',
        'ano',
        'banco',
        'debito',
        'personales_dni',
    ];
    public $timestamps = false;


}
