<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'name', 'date', 'latitude', 'longitude', 'name', 'currencies_id', 'location', 'status'];

    public function currencies() {
      return $this->belongsTo('App\Currency', 'currencies_id');
    }

    public function proposals() {
        return $this->hasMany('App\Proposal', 'events_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'categories_events', 'events_id', 'categories_id');
    }
}
