<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recibo extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'recibos';


    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	"id",
		"codfactura",
		"fecha",
		"codmes",
		"descripcion",
		"importe",
		"importe_original",
		"intereses",
		"codmes_fecha",
		"zona",
		"codoperacion",
		"ano",
		"forma",
		"comprobante_tarjeta",
		"TIPO",
		"lote",

	];

	public function recibo()
    {
    	return $this->belongsTo('App\Factura', 'facturas_id');
    }
}
