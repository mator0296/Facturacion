<!DOCTYPE html>
<html>
	<head>
	    <title>BOLETA DE VENTA</title>
	    <style type="text/css">
	    body{
	        font-size: 16px;
	        font-family: "Arial";
	    }
	    table{
	        border-collapse: collapse;
	    }
	    td{
	        padding: 6px 5px;
	        font-size: 15px;
	    }
	    .h1{
	        font-size: 21px;
	        font-weight: bold;
	    }
	    .h2{
	        font-size: 18px;
	        font-weight: bold;
	    }
			.pull-right{
				right:0px;
			}
			.pull-left{
				left:0px;
			}
		.title:{
			text-align: center;
			font-size: 48px;
			width: 100%;
		}
		.title>span {
			display:inline-block;
			vertical-align:middle;
			line-height:normal;
		}
	    .tabla1{
	        margin-bottom: 20px;
	    }
	    .tabla2 {
	        margin-bottom: 20px;
	    }
	    .tabla3{
	        margin-top: 15px;
	    }
	    .tabla3 td{
	        border: 1px solid #000;
	    }
	    .tabla3 .cancelado{
	        border-left: 0;
	        border-right: 0;
	        border-bottom: 0;
	        border-top: 1px dotted #000;
	        width: 200px;
	    }
			.table4 td{
				font-size: 13px;
			}
	    .emisor{
	        color: red;
					border:0;
					padding-left:250px;
	    }
			.pago{
					padding-top:50px;
	    }
	    .linea{
	        border-bottom: 1px dotted #000;
	    }
	    .border{
	        border: 1px solid #000;
	    }
	    .fondo{
	        background-color: #dfdfdf;
	    }
	    .fisico{
	        color: #fff;
	    }
	    .fisico td{
	        color: #fff;
	    }
	    .fisico .border{
	        border: 1px solid #fff;
	    }
	    .fisico .linea{
	        border-bottom: 1px dotted #fff;
	    }
	    .fisico .emisor{
	        color: #fff;
	    }
	    .fisico .tabla3 .cancelado{
	        border-top: 1px dotted #fff;
	    }
	    .fisico .text{
	        color: #000;
	    }
	    .fisico .fondo{
	        background-color: #fff;
	    }
	    @if($tipo=='fisico')
	    #logo{
	        display: none;
	    }
	    @endif
	</style>
	</head>
	<body>
	    <div class="@if($tipo=='fisico') fisico @endif">
	        <div width="100%" class="tabla1">
				<img src="images/factura/header.png" class="img-fluid pull-xs-left">
	        </div>
	        <div class="title">
        		<span><strong>Comprobante NRO: {{$numero}} TDFSATELITAL </strong></span>
        		<span style="margin-left: 232px;"><strong>FECHA: {{$dia}} / {{$mes}} / {{$ayo}}</strong></span>
        		<br>
        		<span><strong>{{$tipo_factura}}</strong></span>
        		<br>
        		<span style="font-size: 12px;">Original (Cliente)</span>
	        </div>
			<div style="padding:8 0 0 0">
				<div style="width:100%; height:1px; background-color:#616A6B"></div>
			</div>
			<table class="table4">
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>Email:</strong> administracion@tdfsatelital.com.ar </span></td>
						<td style="padding-left:44;"><span class="text"> <strong>Condición de IVA:</strong> Responsable Inscripto </span></td>
				</tr>
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>Sitio Web:</strong> www.tdfsatelital.com.ar </span></td>
						<td style="padding-left:44;"><span class="text"> <strong>Ingresos Brutos</strong> N°109589-7 </span></td>
				</tr>
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>Telefono:</strong> 054-02964-425081 </span></td>
						<td style="padding-left:44;"><span class="text"> <strong>CUIT:</strong> 20246889350 </span></td>
				</tr>
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>Fax:</strong> 054-02964-426361 </span></td>
						<td style="padding-left:44;"><span class="text"> <strong>Inicio de Actividades:</strong> 19/04/2004 </span></td>
				</tr>
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>Domicilio:</strong> Dagostini 1030. Río Grande. Tierra del Fuego </span></td>
						<td style="padding-left:44;"><span class="text"> <strong>Puesto de Venta:</strong> 5 </span></td>
				</tr>
				<tr>
						<td style="padding-left:;"><span class="text"> <strong>RAUL ANGEL VELIS MARIÑO</strong> </span></td>
				</tr>
			</table>
			<div style="padding:2 0 0 0">
				<div style="width:100%; height:1px; background-color:#616A6B"></div>
			</div>
	        <table width="100%" class="table4">
	            <tr>
	                <td style="padding-left:;"><span class="text"><strong>CLIENTE:</strong> {{ $nombres }}</span></td>
	                <td style="padding-left:;"><span class="text"></span></td>
	                <td style="padding-left:44;"><span class="text"><strong>LOCALIDAD: </strong> {{ $localidad}}</span></td>
	                <td style="padding-left:;"><span class="text"></td>
	            </tr>
	            <tr>
	                <td style="padding-left:;"><strong>DIRECCIÓN:</strong> {{ $direccion }}</td>
	                <td style="padding-left:;"><span class="text"></span></td>
	                <td style="padding-left:44;"><strong>DNI:</strong> {{ $dni }}</td>
	                <td style="padding-left:;"><span class="text"></span></td>
	            </tr>
	            <tr>
	            	<td style="padding-left:;"><strong>Condicion de I.V.A: </strong> {{ $condicioniva}}</td>
	                <td style="padding-left:;"><span class="text"></td>
	                <td style="padding-left:44;"><strong>Condiciones de venta: </strong> {{ $condicionventa}}</td>
	                <td style="padding-left:;"><span class="text"></td>
	            </tr>
	        </table>
	        <div style="padding:2 0 0 0">
				<div style="width:100%; height:1px; background-color:#616A6B"></div>
			</div>
	        <table width="100%" class="tabla2">
	            <tr>
	                <td align="center" ><strong>COD</strong></td>
	                <td align="left" ><strong>DESCRIPCIÓN</strong></td>
	                <td align="center" ><strong>SUBTOTAL</strong></td>
	                <td align="center" ><strong>INTERESES</strong></td>
	                <td align="center" ><strong>TOTAL</strong></td>
	            </tr>
	            @for ($i = 0; $i < count($articulos); $i++)
	            @if (array_key_exists($i, $articulos))
	            <tr>
	                <td width="7%" align="center"><span class="text">{{ $articulos[$i]['cod_producto'] }}</span></td>
	                <td width="59%"><span class="text">{{ $articulos[$i]['descripcion'] }}</span></td>
	                <td width="16%" align="right"><span class="text">{{ $articulos[$i]['precio'] }}</span></td>
	                <td width="16%" align="right"><span class="text">0,00</span></td>
	                <td width="18%" align="right"><span class="text">{{ $articulos[$i]['importe'] }}</span></td>
	            </tr>
	            @else
	            <tr>
	                <td width="7%">&nbsp;</td>
	                <td width="59%">&nbsp;</td>
	                <td width="16%">&nbsp;</td>
	                <td width="18%" align="left">&nbsp;</td>
	            </tr>
	            @endif
	            @endfor
	            <tr>
	                <td style="border:0;">&nbsp;</td>
	                <td style="border:0;">&nbsp;</td>
	                
	            </tr>
				</table>
				<div style="width:100%; height:1px; background-color:#616A6B"></div>
				</div>
				<table class="table4">
					<tr>
						<td><strong>IVA = $ 0.00</strong></td>
						<td >
							<strong>Otros Impuestos = $ 0</strong>	
						</td>
						<td >
							<strong>Subtotal = $ {{$total}}</strong>
						</td>
						<td >
							<strong>Pagado = $ {{$total}}</strong>	
						</td>
					</tr>
				</table>
				<div style="padding:0 0 15 0"></div>
				<div style="width:100%; height:1px; background-color:#616A6B"></div>
				</div>
				<table class="table4">
					<tr>
						<td align="center"><strong>NUMERO DE CAE: </strong></td>
						<td >
							<span class="text" width="50%">{{$Cae}}</span>
						</td>
						<td align="center"><strong>Fecha de Emisión: </strong></td>
						<td >
							<span class="text" width="50%">{{$dia}} / {{$mes}} / {{$ayo}}</span>
						</td>
					</tr>
				</table>
				<div style="padding:15 0 15 0"></div>
				<table class="table4">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center"><strong>OFERTA PARA CLIENTES DE TDFSATELITAL</strong></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
				<div style="padding:15 0 15 0"></div>
				<table class="table4">
					<tr>
						<td align="center"><strong>OFERTA IMPERDIBLE</strong></td>
					</tr>
				</table>
				<div style="padding:15 0 15 0"></div>
				<table class="table4">
					<tr>
						<td align="center"><strong>PAGUE A TIEMPO, EVITE CORTES EN EL SERVICIO</strong></td>
					</tr>
				</table>

	    </div>
	</body>
</html>
