<!DOCTYPE html>
<html>
<head>
  <title>RECIBO DE PAGO</title>
  <style type="text/css">
    body{
      background-color:#DEDEDE;
      padding-top:20px;
      padding-bottom:20px;
      font-family:"Arial";
    }
    .page1{
      width:165mm; /*210-40*/
      height:237.4mm; /*297.4-60*/
      background-color:white;
      padding:20px 20px 20px 20px;
      margin-left:auto;
      margin-right:auto;
      box-shadow: 0 0 50px #888888;
    }
    .content{
       padding:20px 20px 20px 20px;
    }

    header{height:15%;border-bottom:1px solid #000000;}
    .div-header {float:left;padding-top:8px;}

    .left-header{
      width:67%;
      border-right:1px solid #000000;
    }
    .right-header{
      width:32%;
    }

    .center-header{width:calc(36% - 2px);border-left:1px solid #000000; border-right:1px solid #000000;}
    section{padding-top:20px;padding-bottom:20px;}
    section{margin:0px;}
    footer{position:absolute; bottom:0;left:0;right:0;padding:20px 0px 20px 0px;} /*Para que el footer llegue hasta abajo*/
    img{margin-bottom:2px;height:85px;max-width:208;}

    .column{height: calc(100% - 8px);}
    .row{left:0;right:0;position:relative;}
    .text-center{text-align:center;}
    .text-left{text-align:left;}
    .text-right{text-align:right;}
    .negrita{font-weight:bold;}
    .preimpreso{color:#999999;text-transform:uppercase;}
    .uppercase{text-transform:uppercase;}
    .h1{font-size:25px;}
    .h2{font-size:20px;}
    .h3{font-size:12px;}
    .h4{font-size:10px;}
    .container{margin:0 20px 0 20px;}
    .bordeRecibo{border:1px solid #000000;margin:0;padding:0;height:100%;position:relative;} /*Para que el footer llegue hasta abajo*/
    .pull-right{position:absolute;right:0;}

    #logo{content: url('/images/recibo/logo1300x600-TDFSATELITAL.png');}

    #tipoComprobante{font-size:50px;}
    #leyendaTipoComprobante{font-size:10px;}
    #tipoComprobante, #lblComprobante{line-height:70%;}
    #lblNroCmp{line-height:200%;}
    #hr{width:30%;border-top:1px solid #000000;height:1px;position:absolute;right:20px;}
    #firma{padding-top:30mm;position:relative;}

    .importeEnPesos:before{content:'$';}

    @media screen{
      body{background-color:#DEDEDE;}
      #page1{box-shadow: 0 0 50px #888888;padding:20mm 18mm 18mm 20mm;}
    }
    @media print{
      body{background-color:#FFFFFF;/*font-family:"Verdana";*/}
      #page1{box-shadow:none;padding:0;}
      button {display:none;}
      #botonera{display:none;}
    }


    /*BOTONERA*/
    button{
      color:#FFFFFF;
      background-color:#428BCA;
      border: 1px solid #357EBD;
      padding:6px 12px;
      margin-bottom:5px;
      text-align: center;
      cursor: pointer;
      font-weight:bold;
    }

    #botonera{
      z-index:999;
      position:absolute;
      left:10px;
      top:10px;
    }
  </style>
  <script>
    /*************************************************************/
// NumeroALetras
// The MIT License (MIT)
//
// Copyright (c) 2015 Luis Alfredo Chee
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// @author Rodolfo Carmona
// @contributor Jean (jpbadoino@gmail.com)
/*************************************************************/
function Unidades(num){

    switch(num)
    {
        case 1: return "UN";
        case 2: return "DOS";
        case 3: return "TRES";
        case 4: return "CUATRO";
        case 5: return "CINCO";
        case 6: return "SEIS";
        case 7: return "SIETE";
        case 8: return "OCHO";
        case 9: return "NUEVE";
    }

    return "";
}//Unidades()

function Decenas(num){

    decena = Math.floor(num/10);
    unidad = num - (decena * 10);

    switch(decena)
    {
        case 1:
            switch(unidad)
            {
                case 0: return "DIEZ";
                case 1: return "ONCE";
                case 2: return "DOCE";
                case 3: return "TRECE";
                case 4: return "CATORCE";
                case 5: return "QUINCE";
                default: return "DIECI" + Unidades(unidad);
            }
        case 2:
            switch(unidad)
            {
                case 0: return "VEINTE";
                default: return "VEINTI" + Unidades(unidad);
            }
        case 3: return DecenasY("TREINTA", unidad);
        case 4: return DecenasY("CUARENTA", unidad);
        case 5: return DecenasY("CINCUENTA", unidad);
        case 6: return DecenasY("SESENTA", unidad);
        case 7: return DecenasY("SETENTA", unidad);
        case 8: return DecenasY("OCHENTA", unidad);
        case 9: return DecenasY("NOVENTA", unidad);
        case 0: return Unidades(unidad);
    }
}//Unidades()

function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
    return strSin + " Y " + Unidades(numUnidades)

    return strSin;
}//DecenasY()

function Centenas(num) {
    centenas = Math.floor(num / 100);
    decenas = num - (centenas * 100);

    switch(centenas)
    {
        case 1:
            if (decenas > 0)
                return "CIENTO " + Decenas(decenas);
            return "CIEN";
        case 2: return "DOSCIENTOS " + Decenas(decenas);
        case 3: return "TRESCIENTOS " + Decenas(decenas);
        case 4: return "CUATROCIENTOS " + Decenas(decenas);
        case 5: return "QUINIENTOS " + Decenas(decenas);
        case 6: return "SEISCIENTOS " + Decenas(decenas);
        case 7: return "SETECIENTOS " + Decenas(decenas);
        case 8: return "OCHOCIENTOS " + Decenas(decenas);
        case 9: return "NOVECIENTOS " + Decenas(decenas);
    }

    return Decenas(decenas);
}//Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    letras = "";

    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + " " + strPlural;
        else
            letras = strSingular;

    if (resto > 0)
        letras += "";

    return letras;
}//Seccion()

function Miles(num) {
    divisor = 1000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMiles = Seccion(num, divisor, "UN MIL", "MIL");
    strCentenas = Centenas(resto);

    if(strMiles == "")
        return strCentenas;

    return strMiles + " " + strCentenas;
}//Miles()

function Millones(num) {
    divisor = 1000000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMillones = Seccion(num, divisor, "UN MILLON DE", "MILLONES DE");
    strMiles = Miles(resto);

    if(strMillones == "")
        return strMiles;

    return strMillones + " " + strMiles;
}//Millones()

function NumeroALetras(num) {
    var data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: "",
        letrasMonedaPlural: 'PESOS',//"PESOS", 'Dólares', 'Bolívares', 'etcs'
        letrasMonedaSingular: 'PESO', //"PESO", 'Dólar', 'Bolivar', 'etc'

        letrasMonedaCentavoPlural: "CENTAVOS",
        letrasMonedaCentavoSingular: "CENTAVO"
    };

    if (data.centavos > 0) {
        data.letrasCentavos = "CON " + (function (){
            if (data.centavos == 1)
                return Millones(data.centavos) + " " + data.letrasMonedaCentavoSingular;
            else
                return Millones(data.centavos) + " " + data.letrasMonedaCentavoPlural;
            })();
    };

    if(data.enteros == 0)
        return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
    if (data.enteros == 1)
        return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
    else
        return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
}//NumeroALetras()
  </script>
</head>

<body>

	<div id="botonera">
    <button onClick="javascript:window.print();">Imprimir</button>
  </div>

  <div id="page1" class="page1">

    <div class="bordeRecibo">

      <header>

        <!-- Lado Izquierdo -->
        <div class="column left-header div-header">
          <div class="container div-header">
            <div class="row div-header">
              <img style="padding-left:40px" id="logo" alt="logo" width="150" height="208">
              <div style="padding-left:5px" class="row text-center h3 negrita">02964-426361 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dagostini 1030. Río Grande. Tierra del Fuego.</div>
            </div>
          </div>
        </div>

        <!-- Lado Derecho -->
        <div class="column right-header div-header">
          <div style="padding-left:32px; padding-top:25px" class="container div-header">
            <div id="lblComprobante" class="row text-center negrita h1 ">RECIBO</div>
            <div id="lblNroCmp" class="row text-center negrita h2 "><span class="preimpreso">Nro</span> 000-{{$numero}}</div>
            <div class="row text-center h3 ">ORIGINAL</div>
          </div>
        </div>
      </header>
        <div class="content">
          <?php
            $hoy = getdate();

            ?>
          <section>

            <br>
             <span>TIERRA DEL FUEGO, {{$dia}} -  {{$mes}} - {{$ayo}}</span>
            <br>
            <br>
            <span class="preimpreso">Recibimos de </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="uppercase">{{$nombre}}</span>
            <br>
            <br>
            <span class="preimpreso">la cantidad de </span>&nbsp;&nbsp;&nbsp;<span class="uppercase">{{$importeString}}</span>
          </section>

          <section id="sectionMedioPago">
            <span class="preimpreso">Mediante</span>
            <div class="row uppercase">
              <span>{{$forma}}</span>
              <span class="pull-right negrita importeEnPesos">{{$importe}}</span>
            </div>
          </section>

          <section>
            <span class="preimpreso">En concepto de</span>
            @for ($i = 0; $i < count($articulos); $i++)
              @if (array_key_exists($i, $articulos))
                <div class="row">
                  <span class="uppercase">{{ $articulos[$i]['descripcion'] }}</span>
                  <span name="a" class="pull-right negrita importeEnPesos">{{ $articulos[$i]['precio'] }}</span>
                </div>
              @else
                <div class="row">
                  <span>&nbsp;</span>
                  <span name="a" class="pull-right negrita importeEnPesos">&nbsp;</span>
                </div>
              @endif
            @endfor

          </section>

          <footer>
            <div class="content">
              <section id="son"> <span class="preimpreso">TOTAl:</span>
                <output id="totalRecibo" class="negrita importeEnPesos">{{$importe}}</output>
              </section>

              <section id="firma">
                <div id="hr" class="pull-right">&nbsp;</div>
                <p class="text-right" style="margin-right: 14px;">TDFSatelital</p>
              </section>
            </div>
          </footer>
        </div><!-- content -->
    </div><!-- bordeRecibo -->
  </div><!-- Page1 -->
  <script>
    (document).ready(function(){
      ('input[ng-model="yourAmount"]').on('input', function(a, b){
        var numero = NumeroALetras((this).val());
        ('#importeEnLetras').html(numero);
      });
    });
  </script>
</body>
</html>
