<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api'

], function ($router) {
    Route::post('authenticate', 'Api\ApiAuthController@authenticate');
    Route::post('register', 'Api\ApiAuthController@register');
    Route::post('login', 'Api\ApiAuthController@login');
    Route::post('logout', 'Api\ApiAuthController@logout');
    Route::post('refresh', 'Api\ApiAuthController@refresh');
    Route::get('me', 'Api\ApiAuthController@me');
    Route::get('auth/facebook', 'Api\ApiFacebookController@redirectToProvider');
    Route::post('auth/facebook/callback', 'Api\ApiFacebookController@handleProviderCallback');
    Route::get('auth/google', 'Api\ApiGoogleController@redirectToProvider');
    Route::post('auth/google/callback', 'Api\ApiGoogleController@handleProviderCallback');
    Route::get('/restricted', 'Api\ApiAuthController@me');
    Route::get('/user','Api\ApiAuthController@getUsers');
    Route::delete('/user', 'Api\ApiAuthController@deleteUsers');
    Route::put('/user', 'Api\ApiAuthController@edit');

    Route::get('/prueba', function(Request $request) { return App\Client::where('id', $request->id)->with('users')->first(); });

    Route::post('/client', 'Api\ApiClientController@Create');
    Route::get('/client', 'Api\ApiClientController@All');
    Route::put('/client', 'Api\ApiClientController@Edit');
    Route::delete('/client', 'Api\ApiClientController@Delete');
    Route::get('/client/{id}', 'Api\ApiClientController@Client');
    Route::post('client-login', 'Api\ApiAuthController@clientLogin');

    Route::post('/factura', 'Api\FacturaController@Create');
    Route::get('/factura', 'Api\FacturaController@All');
    Route::put('/factura', 'Api\FacturaController@Edit');
    Route::delete('/factura', 'Api\FacturaController@Delete');
    Route::get('/productsfactura', 'Api\FacturaController@ProductsFacturas');
    Route::get('/factura/{id}', 'Api\FacturaController@Factura');
    Route::get('/facturaforclient/{dni}', 'Api\FacturaController@FacturaForClient');
    Route::get('/afip/getNumFactura', 'Api\ApiMercadoPagoController@getNumberFactura');
    Route::get('/afip/getIvaFactura', 'Api\ApiMercadoPagoController@getIva');
    Route::get('/afip/getDocFactura', 'Api\ApiMercadoPagoController@getTipoFactura');
    Route::get('/afip/getDataNewFactura', 'Api\ApiMercadoPagoController@getDataNewFactura');
    Route::get('/afip/{id}', 'Api\ApiMercadoPagoController@generarAfipWeb');
    Route::get('/client-invoices/{dni}', 'Api\FacturaController@clientInvoices');
    Route::post('/registerFacturas', 'Api\ApiMercadoPagoController@RegisterPost');
    Route::get('/coupon','Api\FacturaController@coupon');
    Route::post('/generatecoupon','Api\FacturaController@generateCoupon');



    Route::post('/creditcard', 'Api\creditCardController@Create');
    Route::get('/creditcard', 'Api\creditCardController@All');
    Route::get('/client-creditcards', 'Api\creditCardController@GetClientCards');
    Route::put('/creditcard', 'Api\creditCardController@Edit');
    Route::delete('/creditcard', 'Api\creditCardController@Delete');
    Route::get('/creditcard/{id}', 'Api\creditCardController@Creditcard');

    Route::post('/product', 'Api\productController@Create');
    Route::get('/product', 'Api\productController@All');
    Route::put('/product', 'Api\productController@Edit');
    Route::delete('/product', 'Api\productController@Delete');
    Route::get('/product/{id}', 'Api\productController@Product');

    Route::post('/recibo', 'Api\ApiRecibosController@Create');
    Route::get('/recibo', 'Api\ApiRecibosController@All');
    Route::put('/recibo', 'Api\ApiRecibosController@Edit');
    Route::delete('/recibo', 'Api\ApiRecibosController@Delete');
    Route::get('/recibo/{id}', 'Api\ApiRecibosController@Recibo');
    Route::get('/client-receipts/{dni}', 'Api\ApiRecibosController@clientReceipts');

    Route::post('/reclamos', 'Api\ApiReclamosController@Create');
    Route::get('/reclamos', 'Api\ApiReclamosController@All');
    Route::post('/reclamos-update', 'Api\ApiReclamosController@Edit');
    Route::post('/reclamos-delete', 'Api\ApiReclamosController@Delete');
    Route::get('/reclamos/{id}', 'Api\ApiReclamosController@Recibo');
    Route::post('/create-client-ticket', 'Api\ApiReclamosController@createClientTicket');
    Route::post('/update-client-ticket', 'Api\ApiReclamosController@updateClientTicket');
    Route::get('/client-tickets/{dni}', 'Api\ApiReclamosController@clientTickets');

    Route::post('/mercadoPago', 'Api\ApiMercadoPagoController@checkout');
    Route::get('/Ipc', 'Api\ApiMercadoPagoController@Ipc');
    Route::get('/IpcSuccess/{id}/{reference}', 'Api\ApiMercadoPagoController@Success');
    Route::get('/IpcPending/{id}/{reference}', 'Api\ApiMercadoPagoController@Pending');
    Route::get('/IpcFailure/{id}', 'Api\ApiMercadoPagoController@Failed');

    //auxiliar
    Route::get('/updateClients', 'Api\FacturaController@IvaClients');


});
