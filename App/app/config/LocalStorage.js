import { AsyncStorage } from 'react-native';

const getUser = async () => {
	let user = await AsyncStorage.getItem('userFacturacion');
	try {
	    user =  JSON.parse(user);
	    return user;
	} catch (e) {
	    return null;
	}
} 

const setUser = async (data) => {
	try {
		await AsyncStorage.setItem('userFacturacion', JSON.stringify(data));
	} catch (error){
		
	}
}

export default {
	getUser,
	setUser
};