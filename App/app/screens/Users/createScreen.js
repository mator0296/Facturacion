import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../config/Error';
import Firebase from '../../config/firebase';
import user from '../../services/user';


export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1
      
    };
    if(this.props.navigation.state.params) {
      console.log(this.props.navigation.state.params.user)
      this.state = {...this.state, ...this.props.navigation.state.params.user}
    }
    
    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){
    
    if(!this.state.usuario){
      ERROR("Debe introducir un nombre","Error");
      return;
    }
    if(!this.state.email){
      ERROR("Debe introducir una descripcion","Error");
      return;
    }
    if(!this.state.password  && !this.props.navigation.state.params){
      ERROR("Debe introducir un dia","Error");
      return;
    } else if(!this.props.navigation.state.params){
      if(this.state.password !== this.state.repetePassword){
        ERROR("Contrasena con coincide","Error");
        return;
      } 
    }
    

    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      user.updateUser(this.state.token, this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateUsers`).set({value:true});
          ERROR("Usuario actualizado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      user.register(this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateUsers`).set({value:true});
          ERROR("Usuario creada con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }
    
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando usuario ${this.props.navigation.state.params.user.nombre}` : 'Creando usuario'}</Title>
          </Body>
        </Header>    
        <Content>
          <ScrollView style={styles.container}>
            
            <Text style={styles.label}>Usuario</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.usuario}
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('usuario', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>


            <Text style={styles.label}>Email</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.email}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('email', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Nombre</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.nombre}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('nombre', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Contrasena</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              secureTextEntry
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('password', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            <Text style={styles.label}>Repetir Contrasena</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              secureTextEntry
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('repetePassword', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>
          
           
            
              <TouchableOpacity 
                style={styles.button}
                onPress={this.submitValue}
                >
                {
                  this.state.loading ?
                  <ActivityIndicator color="#fff"/>
                  :
                  <Text style={styles.buttonText}>Registrar usuario</Text>  
                }
              </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />
            

          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 0.5)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */