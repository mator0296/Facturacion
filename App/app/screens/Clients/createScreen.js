import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../config/Error';
import Firebase from '../../config/firebase';
import Product from '../../services/product';
import client from '../../services/client';


export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1
      
    };
    if(this.props.navigation.state.params) {
      
      this.state = {...this.state, ...this.props.navigation.state.params.client}
    }
    
    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){
    
    if (!this.state.nombre){
      ERROR("Debe introducir un nombre","Error");
      return;
    }
    if (!this.state.ap){
      ERROR("Debe introducir una apellido","Error");
      return;
    }
    if (!this.state.email){
      ERROR("Debe introducir un email","Error");
      return;
    } 

    
    if (!this.state.ciudad) {
      ERROR("Debe introducir una cuidad ", 'Error');
      return;
    }

    
    if (!this.state.telefono) {
      ERROR("Debe introducir un telefono", 'Error');
      return;
    }

    if (!this.state.dni) {
      ERROR("Debe introducir un dni", 'Error');
      return;
    }

    if (!this.state.zona) {
      ERROR("Debe introducir una zona ", 'Error');
      return;
    }

    if (!this.state.codigo_postal) {
      ERROR("Debe introducir un codigo postal", 'Error');
      return;
    }

    if (!this.state.Barrio) {
      ERROR("Debe introducir un barrio", 'Error');
      return;
    }

    //if (!this.state.fecha_nac) {
      //ERROR("Debe introducir una fecha de nacimiento ", 'Error');
      //return;
    //}

    if (!this.state.domicilio) {
      ERROR("Debe introducir un domicilio", 'Error');
      return;
    }
    

    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      client.Update(this.state.token, this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateClients`).set({value:true});
          ERROR("Cleinte actualizado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      client.Create(this.state.token ,this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateClients`).set({value:true});
          ERROR("Cleinte creado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }
    
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando cliente ${this.props.navigation.state.params.client.nombre}` : 'Creando cliente'}</Title>
          </Body>
        </Header>    
        <Content>
          <ScrollView style={styles.container}>
            
            <Text style={styles.label}>Nombre</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.nombre}
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('nombre', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>


            <Text style={styles.label}>Apellido</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.ap}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('ap', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Dni</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.dni}
              keyboardType='numeric'
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('dni', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Email</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              keyboardType='email-address'
              value={this.state.email}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('email', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Zona</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.zona}
              keyboardType='numeric'
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('zona', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Barrio</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              value={this.state.Barrio}
              onChangeText={(text)=>this.onChangeValue('Barrio', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            <Text style={styles.label}>Domicilio</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              value={this.state.domicilio}
              onChangeText={(text)=>this.onChangeValue('domicilio', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>
            
            <Text style={styles.label}>Telefono</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              keyboardType='numeric'
              value={this.state.telefono}
              onChangeText={(text)=>this.onChangeValue('telefono', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            <Text style={styles.label}>Codigo postal</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              keyboardType='numeric'
              value={this.state.codigo_postal}
              onChangeText={(text)=>this.onChangeValue('codigo_postal', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            <Text style={styles.label}>Ciudad</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              placeholderTextColor='#ccc'
              value={this.state.ciudad}
              onChangeText={(text)=>this.onChangeValue('ciudad', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>
            <TouchableOpacity 
              style={styles.button}
              onPress={this.submitValue}
              >
              {
              this.state.loading ?
              <ActivityIndicator color="#fff"/>
              :
              <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar cliente" : "Registrar cliente"}</Text>  
            }
            </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />
            

          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */