import React from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import CreditCard from '../../../services/creditCard';
import Firebase from '../../../config/firebase';

export default class CreditCards extends React.Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      basic: true,
      personales_dni:this.props.screenProps.user.user.dni,
      products:[]
    };

    this.getData = this.getData.bind(this);
    this.goToProductDetail = this.goToProductDetail.bind(this);

  }

  getData(){
    this.setState({refreshing:true})
    CreditCard.GetClientCards(this.state.personales_dni)
      .then(data=>{
        this.setState({refreshing:false ,products: data ? data.reverse() : []})
      });
  }

  goToProductDetail(product) {
    this.props.navigation.navigate('CreditCardDetails', {product})
  }

  componentDidMount(){
    this.getData()
  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.products];
    const dataDelete = newData.splice(rowId, 1)[0]
    CreditCard.Delete(dataDelete.id)
      .then(data=>console.log(data))
    this.setState({ products: newData });
  }

  render() {

    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>{"Tarjetas de Crédito"}</Title>
          </Body>
          <Right>

          </Right>
        </Header>
        <Content>
          <List
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(this.state.products)}
            renderRow={(data,i) =>
              <TouchableOpacity key={i} style={[styles.evento, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onPress={()=>this.goToProductDetail(data)}>
                <Text style={styles.eventoTitulo}>{`Nombre: ${data.nombretarjeta}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Numero: ${data.numerotarjeta}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Banco: ${data.banco}`}</Text>
                <Text style={styles.eventoFecha}>{`Vencimiento: ${data.vencimientotarjeta}`}</Text>
              </TouchableOpacity>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('AddCreditCard', {product:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
        <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate('AddCreditCard')}>
            <Icon name="md-add" />

          </Fab>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToProductDetail(data)}>
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});
