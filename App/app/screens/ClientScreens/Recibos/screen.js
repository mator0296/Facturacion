import React from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import Recibos from '../../../services/recibo';
import Firebase from '../../../config/firebase';

export default class ClientReceipts extends React.Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      basic: true,
      recibos:[]
    };
    this.getData = this.getData.bind(this);

    this.goToReciboDetail = this.goToReciboDetail.bind(this);
  }
  componentDidMount() {
      this.ref = Firebase.database().ref(`update/${this.props.screenProps.user.user['\ufeffid']}`)
        .on('value', (dataSnapshot) => {
            const data = dataSnapshot.val();
            if(data){
              if(data.updateRecibos){
                this.getData();
                Firebase.database().ref(`update/${this.props.screenProps.user.user['\ufeffid']}/updateRecibos`).set(null);
              }
            }

        });
      this.getData();
  }



  getData(){
    this.setState({refreshing:true})
    Recibos.ClientReceipts(this.props.screenProps.user.user.dni)
      .then(data=>{
        this.setState({refreshing:false ,recibos: data ? data.reverse() : []})
      });


  }

  goToReciboDetail(recibo) {

      this.props.navigation.navigate('ReceiptDetails', {recibo})


  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.recibos];
    const dataDelete = newData.splice(rowId, 1)[0]
    Recibos.Delete(this.props.screenProps.user.token,dataDelete.id)
      .then(data=>console.log(data))
    this.setState({ recibos: newData });
  }

  render() {
    console.log(this.state.recibos)
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>{"Recibos"}</Title>
          </Body>
          <Right>

          </Right>
        </Header>
        <Content>
          <List
            dataSource={this.ds.cloneWithRows(this.state.recibos)}
            renderRow={(data,i) =>
              <TouchableOpacity key={i} style={[styles.evento, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onPress={()=>this.goToReciboDetail(data)}>
                <Text style={styles.eventoTitulo}>{`Descripcion: ${data.descripcion}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Importe: ${data.importe}`}</Text>
              </TouchableOpacity>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('ReciboCreate', {recibo:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToReciboDetail(data)}>
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});
