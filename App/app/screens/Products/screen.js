import React from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import product from '../../services/product';
import Firebase from '../../config/firebase';
export default class productScreen extends React.Component {


  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    
    this.state = {
      basic: true,
      products:[]
    };
    this.getData = this.getData.bind(this);
    
    this.goToProductDetail = this.goToProductDetail.bind(this);
  }
  componentDidMount() {
      this.ref = Firebase.database().ref(`update/${this.props.screenProps.user.user.id}`)
        .on('value', (dataSnapshot) => { 
            const data = dataSnapshot.val();
            if(data){
              if(data.updateProducts){
                this.getData();
                Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateProducts`).set(null);
              }  
            }
            
        });
      this.getData();
  }
  
  

  getData(){
    this.setState({refreshing:true})
    product.All(this.props.screenProps.user.token)
      .then(data=>{
        this.setState({refreshing:false ,products: data ? data.reverse() : []})
      });
    
    
  }

  goToProductDetail(product) {
    
      this.props.navigation.navigate('ProductDetail', {product})
    
    
  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.products];
    const dataDelete = newData.splice(rowId, 1)[0]
    product.Delete(this.props.screenProps.user.token,dataDelete.id)
      .then(data=>console.log(data))
    this.setState({ products: newData });
  }

  render() {
    
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>{"Productos"}</Title>
          </Body>
          <Right>
           
          </Right>
        </Header>
        <Content>
          <List
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(this.state.products)}
            renderRow={(data,i) =>
              <TouchableOpacity key={i} style={[styles.evento, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onPress={()=>this.goToProductDetail(data)}>                
                <Text style={styles.eventoTitulo}>{`Codigo: ${data.codigo_art}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Detalle: ${data.detalle_art}`}</Text>
                <Text style={styles.eventoFecha}>{`Precio: ${data.unitario}`}</Text>
              </TouchableOpacity>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('ProductCreate', {product:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
        <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate('ProductCreate')}>
            <Icon name="md-add" />
            
          </Fab>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToProductDetail(data)}>                
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>  
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});


