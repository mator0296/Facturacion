import React from 'react';
import { Button, Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar, ActivityIndicator } from 'react-native';
import User from '../services/user';
import Client from '../services/client';
import ERROR from '../config/Error';
import Select from 'react-native-picker-select';

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header:null
  }

  constructor(props) {
    super(props);

    this.state = {
      'Email':'',
      'Password':'',
      'activity':false,
      role:'Cliente',
    };
  }

  onChangeValue(name, value){
    this.setState({[name]:value})
  }

  _validLogin(){
    let bandera = false;

    if (this.state.Email === "") {
      bandera = true;
      ERROR("Debe introducir un correo","Error");
      return;

    }

    if (this.state.Password === "") {
      bandera = true;
      ERROR("Debe introducir una contraseña", "Error");
      return;
    }

    if (!bandera) {
      this.setState({activity:true})
      if (this.state.role === 'Usuario') {
        User.login(this.state.Email, this.state.Password)
        .then(
          (dataToken) => {
            console.log(dataToken)
            if(!dataToken.error){
              User.me(dataToken.access_token)
                .then(dataUser=>{
                  dataUser.user.password = this.state.Password;
                  this.props.screenProps.logIn({token:dataToken.access_token, user:dataUser.user, role:this.state.role});
                  this.setState({activity:false})
                })
            }else {
              ERROR("Credenciales inválidas", "Error");
              this.setState({activity:false})
              return;
            }
          }
        )
      } else if (this.state.role === 'Cliente') {
        Client.login(this.state.Email, this.state.Password)
        .then(
          (data) => {
            console.log(data)
            if(!data.error){
              this.props.screenProps.logIn({user:data, role:this.state.role});
              this.setState({activity:false})
            }else {
              ERROR("Credenciales inválidas", "Error");
              this.setState({activity:false})
              return;
            }
          }
        )
        .catch(error=>{
          ERROR("Credenciales inválidas", "Error");
              this.setState({activity:false})
              return;
        })
      }
    }
  }

  render() {

    const items = ["Usuario", "Cliente"];

    return (
      <View style={styles.container}>
        <StatusBar barStyle = "light-content" backgroundColor="#000" />
        <Text
          style={styles.logo}
          source={require('../img/logo.png')}
        >{"FACTURACION"}</Text>

        <View style={{backgroundColor:'#888', width:'100%', marginTop:'-15%',marginBottom:'15%', borderRadius:5}}>
          <TextInput
            style={styles.input}
            placeholder="Correo electrónico"
            placeholderTextColor='rgba(255,255,255, 0.7)'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('Email', text)}
            >
          </TextInput>

          <TextInput
            style={styles.input}
            placeholder="Dni"
            secureTextEntry
            placeholderTextColor= 'rgba(255,255,255, 0.7)'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('Password', text)}
            >

          </TextInput>

          {/*<View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
  					<Select
  		        placeholder={{
  		          label: 'Seleccione rol',
  		          value: null,
  		        }}
  		        items={items.map(data=>({label:data, value:data, color:'black'}))}
  		        onValueChange={(value) => this.setState({role:value})}
  		        value={this.state.role}
  		      />
          </View>*/}
        </View>

        <TouchableOpacity
          style={styles.button}
          onPress={this._validLogin.bind(this)}
          >
          {!this.state.activity ?
          <Text style={styles.buttonText}>Ingresar</Text>
          :
          <ActivityIndicator size="small" color="#fff" />
          }
        </TouchableOpacity>

        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#888",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },

  input: {
    padding: 10,
    backgroundColor:'rgba(0,0,0,0.1)',
    height: 50,
    alignSelf: 'stretch',
    color:"#FFF",
    fontSize: 17,
    marginBottom: 30,
    borderRadius: 3,
  },


  logo : {
    width: 200,
    height: 150,
    marginTop:50,
    textAlign:'center',
    fontSize:17,
    fontWeight:'bold'
  },

  button : {
    height: 50,
    backgroundColor:"#2C2C2C",
    // backgroundColor:"#feda73",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
  },

  buttonSecondary : {
    height: 50,
    //backgroundColor:"#2CA8FF",
    //backgroundColor:"#feda73",
    backgroundColor:"#fec53d",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
    marginTop: 30
  },


  buttonText : {
    color: "#F96332",
    textAlign:'center',
    fontSize: 17,
  },

  link : {
    marginTop: 30,
  }
});
