import React from 'react';
import { Button, Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar } from 'react-native';

export default class RecuperarScreen extends React.Component {
  static navigationOptions = {
    header:null
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle = "light-content" backgroundColor="#000" />
        <Image
          style={styles.logo}
          source={require('../img/logo.png')}
        /> 
        
        <TextInput 
          style={styles.input}
          placeholder="Correo Electrónico"
          placeholderTextColor='rgba(255,255,255, 0.7)'
          underlineColorAndroid={'rgba(0,0,0,0)'}
          >
        </TextInput>

       
        <TouchableOpacity 
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Login')}
          >
          <Text style={styles.buttonText}>Recuperar</Text>  
        </TouchableOpacity>

        <TouchableOpacity 
          style={styles.link}
          onPress={() => this.props.navigation.navigate('Login')}
          >
          <Text style={styles.buttonText}>Iniciar Sesión</Text>  
        </TouchableOpacity>  

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },

  input: {
    padding: 10,
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    height: 50,
    alignSelf: 'stretch',
    color:"#fff",
    fontSize: 17,
    marginBottom: 30,
    borderRadius: 3,
  },


  logo : {
    width: 200,
    resizeMode: 'contain'
  },

  button : {
    height: 50,
    backgroundColor:"#ea1d75",
    // backgroundColor:"#feda73",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
  },

  buttonText : {
    color: "#fff",
    textAlign:'center',
    fontSize: 17,
  },

  link : {
    marginTop: 30,
  }




});


