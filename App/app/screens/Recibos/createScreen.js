import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../config/Error';
import Firebase from '../../config/firebase';
import Recibos from '../../services/recibo';
import Client from '../../services/client';
import Facturas from '../../services/factura';
import Select from 'react-native-picker-select';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';

export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1,
      clients:[],
      facturas:[],
      datachargue:true,
    };
    if(this.props.navigation.state.params) {
      this.state.datachargue = false;
      this.state = {...this.state, ...this.props.navigation.state.params.recibo}
      Facturas.FacturaForClient(this.props.navigation.state.params.recibo.facturas_personales_id,this.state.token)
        .then((data)=>this.setState({facturas:data}));
    }
    
    

    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
   

    if (name === 'facturas_personales_id') {
      Facturas.FacturaForClient(value,this.state.token)
        .then(data=>this.setState({facturas:data}))
    }
    this.setState({[name]:value})
  }

  componentWillMount(){
    Client.All(this.state.token)
      .then(clients=>this.setState({clients}))
  }

  submitValue(){
    
    if(!this.state.fecha){
      ERROR("Debe introducir un fecha","Error");
      return;
    }
    if(!this.state.descripcion){
      ERROR("Debe introducir una descripcion","Error");
      return;
    }

    
    if(!this.state.forma){
      ERROR("Debe introducir un tipo de pago","Error");
      return;
    } 

    if(!this.state.facturas_personales_id){
      ERROR("Debe elegir un cliente","Error");
      return;
    } 

    if(!this.state.facturas_id){
      ERROR("Debe elegir una factura","Error");
      return;
    } 
    

    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      Recibos.Update(this.state.token, this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateRecibos`).set({value:true});
          ERROR("Recibo actualizado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      Recibos.Create(this.state.token ,this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateRecibos`).set({value:true});
          ERROR("Recibo creado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }
    
  }

  render() {
    console.log(this.state)
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando recibo ${this.props.navigation.state.params.recibo.id}` : 'Creando Recibo'}</Title>
          </Body>
        </Header>    
        <Content>
          <ScrollView style={styles.container}>
            
            <Text style={styles.label}>Fecha</Text>
            <TouchableOpacity onPress={(text)=>this.onChangeValue('isDateTimePickerVisible', true)}>
              <TextInput 
                style={styles.input}
                placeholder=""
                value={this.state.fecha}
                keyboardType='numeric'
                placeholderTextColor= '#ccc'
                editable={false}
                
                underlineColorAndroid={'#2CA8FF'}>
                
              </TextInput>
            </TouchableOpacity>

            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={(text)=>this.onChangeValue('fecha', Moment(text.toString()).format('YYYY-MM-DD'))}
              onCancel={(text)=>this.onChangeValue('isDateTimePickerVisible', false)}
            />


            <Text style={styles.label}>Descripcion</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.descripcion}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('descripcion', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            

            <Text style={styles.label}>Tipo de pago</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona un el tipo de pago...',
                      value: null,
                  }}
                  items={[{label:'efectivo', value:'efectivo', color:'black'},{label:'Tarjeta de credito', value:'tarjeta de credito', color:'black'}]}
                  onValueChange={(value) => this.onChangeValue('forma', value)}
                  value={this.state.forma}
                  
              />
            </View>

            <Text style={styles.label}>Clientes</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona un cliente...',
                      value: null,
                  }}
                  items={this.state.clients.map(data=>({label:data.nombre, value:data.id, color:'black'}))}
                  onValueChange={(value) => this.onChangeValue('facturas_personales_id', value)}
                  value={this.state.facturas_personales_id}
                  
              />
            </View>
            
            <Text style={styles.label}>Facturas</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona una factura...',
                      value: null,
                  }}
                  items={this.state.facturas.map(data=>({label:data.num_factura, value:data.id, color:'black'}))}
                  onValueChange={(value) => this.onChangeValue('facturas_id', value)}
                  value={this.state.facturas_id}
                  
              />
            </View>

            <Text style={styles.label}>Importe</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.importe}
              keyboardType='numeric'
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('importe', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>
            
            
            <TouchableOpacity 
              style={styles.button}
              onPress={this.submitValue}
              >
              {
              this.state.loading ?
              <ActivityIndicator color="#fff"/>
              :
              <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar recibo" : "Registrar recibo"}</Text>  
            }
            </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />
            

          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */