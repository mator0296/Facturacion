import Base from './base';

class User extends Base {

	login = (email, password)=>{
		return this.post('login', { email, password });
	}

	register = (data)=>{
		return this.post('register', data);
	}

	me = (token) => (
		this.get('me',{token} )
	)

	refresh = (token)=>{
		return this.post('refresh', {token});
	}

	getUsers = (token, page) => {
		return this.get('user', {token, page});
	}

	deleteUsers = (token, id) => {
		return this.delete('user', {token, id});
	}

	updateUser = (token, data) => {
		return this.put('user', {...data, token});
	}
}

export default new User;