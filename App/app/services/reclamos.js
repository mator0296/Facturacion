import Base from './base';



class Recibo extends Base {

	All = (token, page) => {
		return this.get('reclamos',{token, page});
	}

	Create = (token , data) => {
		return this.post('reclamos',{token, ...data});
	}

	Get = ( token, id ) => {
		return this.get('reclamos/' + id, {token});
	}

	Delete = (token, id) => {
		return this.post('reclamos-delete', {token, id});
	}

	Update = (token, data) => {
		return this.post('reclamos-update', {...data, token});
	}

	GetClientTickets = ( dni ) => {
		return this.get('client-tickets/' + dni, {});
	}

	CreateClientTicket = (data) => {
		return this.post('create-client-ticket',{...data});
	}

	UpdateClientTicket = (data) => {
		return this.post('update-client-ticket', {...data});
	}
}

export default new Recibo;
