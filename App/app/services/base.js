import axios from 'axios';
export default class base {
	constructor(props) {
	  
	  	this.url = 'https://facturacionblue.000webhostapp.com/api';
	  	this.geturi = this.geturi.bind(this);
	  	this.get = this.get.bind(this);
	  	this.geturl = this.geturi.bind(this);
	  	this.post = this.post.bind(this);
	  	this.headers = {
	  		'Content-type':'application/json',
	  		'Accept':'application/json',
	  	}
		
	}

	get = (uri, params) => {

		return fetch(this.geturi(uri, params), {
			method:'GET',
			header:this.headers
		}).then(resp=>resp.json());
    
	}

	post = async (uri, params) => {
		
		return axios.post(this.geturi(uri,params), params).then(res=>res.data);
		
		

	}

	delete = async (uri, params) => {
		
		return axios.delete(this.geturi(uri,params), params).then(res=>res.data);
		
		

	}

	put = (uri, params) => {
		
		return fetch(this.geturi(uri,params), {
			method:'PUT',
			header:this.headers,
			body:JSON.stringify(params)
		}).then(resp=>resp.json());

	}

	geturi = (uri, params) =>{
		let url = `${this.url}/${uri}`
	
    const query = Object.keys(params).map(k => {
        if (Array.isArray(params[k])) {
            return params[k]
                .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
                .join('&')
        }
        return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
    }).join('&')
    if (query) {
        url = `${url}?${query}`
    }

    console.log('URL', url.toString())
    return url.toString()
	}

	geturl = (url, params) =>{
        const query = Object.keys(params).map(k => {
            if (Array.isArray(params[k])) {
                return params[k]
                    .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
                    .join('&')
            }
            return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
        }).join('&')
        if (query) {
            url = `${url}?${query}`
        }

        console.log('URL', url.toString())
        return url.toString()
	}

}