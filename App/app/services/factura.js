import Base from './base';

class Factura extends Base {

	All = (token, page, search, value) => {
		return this.get('factura',{token, page, search, value});
	}

	Create = (token , data) => {
		return this.post('factura',{token, ...data});
	}

	Get = ( token, id ) => {
		return this.get('factura/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('factura', {token, id});
	}

	Update = (token, data) => {
		return this.put('factura', {...data, token});
	}

	FacturaForClient = (id,token) => {

		return this.get('facturaforclient/'+ id, {token});
	}

	ClientInvoices = (dni) => {

		return this.get('client-invoices/'+ dni, {});
	}

	MercadoPago = (data) => {
		return this.post('mercadoPago', data);
	}

	GenerateCupo = (data) => {
		return this.post('registerFacturas', data);
	}

	GetDocumentTypes = () => {
		return this.get('afip/getDocFactura',{});
	}
}

export default new Factura;
